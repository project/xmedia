/**
 * @file
 * Xmedia ckeditor dialog.
 */

( function(){
	var xmediaDialog = function(editor){
		return {
        title : 'Xmedia',
        minWidth : 800,
        minHeight : 500,
        contents :
          [
            {
              id : 'iframe',
              label : 'Xmedia Browser',
              expand : true,
              elements :
              [
                {
                  type : 'iframe',
                  src : Drupal.settings.basePath + 'xmedia-browser/wizard',
                  width : '100%',
                  height : '100%',
                  onContentLoad : function() {
                    iframeElement = document.getElementById( this._.frameId );
                    iframeWindow = iframeElement.contentWindow;
                  }
                }
              ]
            }
          ],
        onOk : function()
        {
          if(iframeWindow.Drupal.settings.xmediaBrowser.mode == 'wizard') {
            if(iframeWindow.Drupal.settings.xmediaBrowser.selectedImage != null) {
              var xmedia_selected_id = iframeWindow.Drupal.settings.xmediaBrowser.selectedImage;
              iframeElement.src = Drupal.settings.basePath + 'xmedia-browser/wizard/insert/' + xmedia_selected_id;
            } else {
              alert(Drupal.t('Please select an image to crop.'));
            }
            return false;
          } else if(iframeWindow.Drupal.settings.xmediaBrowser.mode == 'insert') {
            var publicPath = iframeWindow.document.getElementsByName('public_path')[0].value;
            var imagePath = iframeWindow.document.getElementsByName('image_path')[0].value;
            var insertImageSrc = publicPath + imagePath;
            var insertImageAlt = iframeWindow.document.getElementsByName('alt')[0].value;
            var insertImageTitle = iframeWindow.document.getElementsByName('title')[0].value;
            
            if(typeof(iframeWindow.document.getElementsByName('crop_coords')[0]) != 'undefined' && iframeWindow.document.getElementsByName('crop_coords')[0].value != '') {
              var cropCoords = iframeWindow.document.getElementsByName('crop_coords')[0].value;
              var cropStyleOrigin = iframeWindow.document.getElementsByName('crop_style_origin')[0].value;
              insertImageSrc = publicPath + 'xmedia_crop/' + cropStyleOrigin + '/' + cropCoords + '/' + imagePath;
            }
            
            this._.editor.insertHtml('<img src="' + insertImageSrc + '" alt="' + insertImageAlt + '" title="' + insertImageTitle + '" />');
          }
        }
     }
	}
	
	CKEDITOR.dialog.add('xmedia', function(editor) {
		return xmediaDialog(editor);
	});
		
})();