/**
 * @file
 * Plugin for inserting medias from Xmedia library.
 */

(function ($) {
  var iframeElement = null;
  var iframeWindow = null;
  CKEDITOR.plugins.add( 'xmedia', {
    requires : ['iframedialog'],

    init: function( editor ) {
      var pluginName = 'xmedia';
            
      CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/xmedia.js');
      
      editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
      
      editor.ui.addButton( pluginName, {
        label: 'Xmedia',
        command: pluginName,
        icon: this.path + 'xmedia.png'
      });
    }

  });

})(jQuery);
