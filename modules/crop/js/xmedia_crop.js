Drupal.behaviors.xmediaCrop = {
  attach:function (context, settings) {
    if (!settings.xmediaCrop) {
      return;
    }

    // Apply the MyBehaviour effect to the elements only once.
    var pane = jQuery('#' + settings.xmediaCrop.wrapper_id, context);
    pane.once('crop', function () {

      var init_coords = settings.xmediaCrop.coords;
      var cropImage = jQuery('img.jcrop-image', pane);
      var formsContext = pane.parent();

      if(settings.xmediaCrop.ratio == 1) {
        var aspectRatio = (init_coords.x1 - init_coords.x) / (init_coords.y1 - init_coords.y);
      } else if(settings.xmediaCrop.ratio == 0) {
        var aspectRatio = false;
      }
            
      if(jQuery(formsContext).parent().parent().parent().find('.image_style').val()) {
                
        jQuery(cropImage).Jcrop({
          'onChange':function (coords) {
            var image_style = jQuery(formsContext).parent().parent().parent().find('.image_style').val();
            
            jQuery('input.x.' + image_style, formsContext).attr('value', coords.x);
            jQuery('input.changed.' + image_style, formsContext).attr('value', 1);
            jQuery('input.y.' + image_style, formsContext).attr('value', coords.y);
            jQuery('input.x1.' + image_style, formsContext).attr('value', coords.x2);
            jQuery('input.y1.' + image_style, formsContext).attr('value', coords.y2);
            jQuery('input.height.' + image_style, formsContext).attr('value', coords.h);
            jQuery('input.width.' + image_style, formsContext).attr('value', coords.w);

            jQuery('input.jcrop-x', formsContext).attr('value', coords.x);
            jQuery('input.jcrop-y', formsContext).attr('value', coords.y);
            jQuery('input.jcrop-x1', formsContext).attr('value', coords.x2);
            jQuery('input.jcrop-y1', formsContext).attr('value', coords.y2);
            jQuery('input.jcrop-height', formsContext).attr('value', coords.h);
            jQuery('input.jcrop-width', formsContext).attr('value', coords.w);
          },
          'onSelect':function (coords) {
            var image_style = jQuery(formsContext).parent().parent().parent().find('.image_style').val();
            
            jQuery('input.x.' + image_style, formsContext).attr('value', coords.x);
            jQuery('input.changed.' + image_style, formsContext).attr('value', 1);
            jQuery('input.y.' + image_style, formsContext).attr('value', coords.y);
            jQuery('input.x1.' + image_style, formsContext).attr('value', coords.x2);
            jQuery('input.y1.' + image_style, formsContext).attr('value', coords.y2);
            jQuery('input.height.' + image_style, formsContext).attr('value', coords.h);
            jQuery('input.width.' + image_style, formsContext).attr('value', coords.w);
            
            jQuery('input.jcrop-x', formsContext).attr('value', coords.x);
            jQuery('input.jcrop-y', formsContext).attr('value', coords.y);
            jQuery('input.jcrop-x1', formsContext).attr('value', coords.x2);
            jQuery('input.jcrop-y1', formsContext).attr('value', coords.y2);
            jQuery('input.jcrop-height', formsContext).attr('value', coords.h);
            jQuery('input.jcrop-width', formsContext).attr('value', coords.w);
          },
          'aspectRatio': aspectRatio,
          'setSelect':[init_coords.x, init_coords.y, init_coords.x1, init_coords.y1]
        });
      }
    });

    pane.removeOnce('crop', function () {
      });
        
    jQuery('form#xmedia-ui-insert-form').each(function(){
      var form = this;
      var coordsInput = jQuery('input[name="crop_coords"]', form);
      var currentCoords = {};
      var JcropApi;
      jQuery('#edit-crop-area img', this).Jcrop({
        'onChange':function (coords) {
          jQuery(coordsInput).val(coords.x + 'x' + coords.y + 'x' + coords.x2 + 'x' + coords.y2);
          currentCoords = coords;
        },
        'onSelect':function (coords) {
          jQuery(coordsInput).val(coords.x + 'x' + coords.y + 'x' + coords.x2 + 'x' + coords.y2);
          currentCoords = coords;
        }
      }, function(){
        JcropApi = this;
      });
      jQuery('#edit-image-style').change(function(){
        var style;
        var aspectRatio = false;
        if(typeof(settings.xmediaCrop.styles[jQuery(this).val()]) != 'undefined') {
          style = settings.xmediaCrop.styles[jQuery(this).val()];
          JcropApi.destroy();
          if(jQuery('#edit-lock-aspect-ratio').is(':checked')) {
            aspectRatio = (style.x1 - style.x) / (style.y1 - style.y);
          }
          jQuery('#edit-crop-area img', form).Jcrop({
            'onChange':function (coords) {
              jQuery(coordsInput).val(coords.x + 'x' + coords.y + 'x' + coords.x2 + 'x' + coords.y2);
              currentCoords = coords;
            },
            'onSelect':function (coords) {
              jQuery(coordsInput).val(coords.x + 'x' + coords.y + 'x' + coords.x2 + 'x' + coords.y2);
              currentCoords = coords;
            },
            'setSelect':[style.x, style.y, style.x1, style.y1],
            'aspectRatio' : aspectRatio
          }, function(){
            JcropApi = this;
          });
        }
      });
      jQuery('#edit-lock-aspect-ratio').change(function(){
        var aspectRatio = false;
        if(typeof(currentCoords.x) != 'undefined') {
          JcropApi.destroy();
          if(jQuery(this).is(':checked')) {
            aspectRatio = (currentCoords.x2 - currentCoords.x) / (currentCoords.y2 - currentCoords.y);
          }
          jQuery('#edit-crop-area img', form).Jcrop({
            'onChange':function (coords) {
              jQuery(coordsInput).val(coords.x + 'x' + coords.y + 'x' + coords.x2 + 'x' + coords.y2);
              currentCoords = coords;
            },
            'onSelect':function (coords) {
              jQuery(coordsInput).val(coords.x + 'x' + coords.y + 'x' + coords.x2 + 'x' + coords.y2);
              currentCoords = coords;
            },
            'setSelect':[currentCoords.x, currentCoords.y, currentCoords.x2, currentCoords.y2],
            'aspectRatio' : aspectRatio
          }, function(){
            JcropApi = this;
          });
        }
      });
    });
  }
}


// We need to initialize default crop states
Drupal.behaviors.xmediaCropDefault = {
  attach:function (context, settings) {

    // Apply the MyBehaviour effect to the elements only once.
    jQuery('.crop-image-set').each(function (index, values) {
      var pane = jQuery(this);
      pane.once('cropDefault', function () {
        var dcoords = null;
        var cropImage = jQuery('img.jcrop-image', pane);

        var x = jQuery('input.jcrop-x', pane).attr('value');
        var y = jQuery('input.jcrop-y', pane).attr('value');
        var x1 = jQuery('input.jcrop-x1', pane).attr('value');
        var y1 = jQuery('input.jcrop-y1', pane).attr('value');
        var height = jQuery('input.jcrop-height', pane).attr('value');
        var width = jQuery('input.jcrop-width', pane).attr('value');
        
        var ratioElement = jQuery(pane).parent().parent().find('input.ratio');
        if((jQuery(ratioElement).attr('type') == 'checkbox' && jQuery(ratioElement).is(':checked')) || (jQuery(ratioElement).attr('type') == 'hidden' && jQuery(ratioElement).val() == 1)) {
          var aspectRatio = (x1 - x) / (y1 - y);
        } else {
          var aspectRatio = false;
        }
        if(jQuery(pane).parent().parent().parent().find('.image_style').val()) {
          jQuery(cropImage).Jcrop({
            'onChange':function (coords) {
              var image_style = jQuery(pane).parent().parent().parent().find('.image_style').val();

              jQuery('input.x.' + image_style, pane).attr('value', coords.x);
              jQuery('input.changed.' + image_style, pane).attr('value', 1);
              jQuery('input.y.' + image_style, pane).attr('value', coords.y);
              jQuery('input.x1.' + image_style, pane).attr('value', coords.x2);
              jQuery('input.y1.' + image_style, pane).attr('value', coords.y2);
              jQuery('input.height.' + image_style, pane).attr('value', coords.h);
              jQuery('input.width.' + image_style, pane).attr('value', coords.w);
              
              jQuery('input.jcrop-x', pane).attr('value', coords.x);
              jQuery('input.jcrop-y', pane).attr('value', coords.y);
              jQuery('input.jcrop-x1', pane).attr('value', coords.x2);
              jQuery('input.jcrop-y1', pane).attr('value', coords.y2);
              jQuery('input.jcrop-height', pane).attr('value', coords.h);
              jQuery('input.jcrop-width', pane).attr('value', coords.w);
            },
            'onSelect':function (coords) {
              var image_style = jQuery(pane).parent().parent().parent().find('.image_style').val();

              jQuery('input.x.' + image_style, pane).attr('value', coords.x);
              jQuery('input.changed.' + image_style, pane).attr('value', 1);
              jQuery('input.y.' + image_style, pane).attr('value', coords.y);
              jQuery('input.x1.' + image_style, pane).attr('value', coords.x2);
              jQuery('input.y1.' + image_style, pane).attr('value', coords.y2);
              jQuery('input.height.' + image_style, pane).attr('value', coords.h);
              jQuery('input.width.' + image_style, pane).attr('value', coords.w);
              
              jQuery('input.jcrop-x', pane).attr('value', coords.x);
              jQuery('input.jcrop-y', pane).attr('value', coords.y);
              jQuery('input.jcrop-x1', pane).attr('value', coords.x2);
              jQuery('input.jcrop-y1', pane).attr('value', coords.y2);
              jQuery('input.jcrop-height', pane).attr('value', coords.h);
              jQuery('input.jcrop-width', pane).attr('value', coords.w);
            },
            'aspectRatio': aspectRatio,
            'setSelect':[x, y, x1, y1]
          });
        }
      });
    });
  }
}
