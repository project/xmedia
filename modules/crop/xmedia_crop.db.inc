<?php

/**
 * Save or update media's crop settings
 *
 * @param XMediaCrop $xmedia_crop
 * @return bool|XMediaCrop
 */
function xmedia_crop_db_save(XMediaCrop $xmedia_crop) {
  $transaction = db_transaction();

  try {
    // UPDATE
    if (!empty($xmedia_crop->id)) {
      $xmedia_crop->modified = time();
      drupal_write_record('xmedia_crops', $xmedia_crop, 'id');

    }
    else {
      // INSERT
      $xmedia_crop->created = time();
      $xmedia_crop->modified = time();
      drupal_write_record('xmedia_crops', $xmedia_crop);
    }

    return $xmedia_crop;

  } catch (PDOException $e) {
    $transaction->rollback();
    watchdog_exception(WATCHDOG_ERROR, $e, t('Can not save media crop!'));
    return FALSE;
  }
}

function xmedia_crop_db_delete(XMediaCrop $xmedia_crop) {
  $transaction = db_transaction();

  try {
    if (!empty($xmedia_crop->id)) {
      db_delete('xmedia_crops')
        ->condition('id', $xmedia_crop->id)
        ->execute();
    }
    return TRUE;
  } catch (PDOException $e) {
    $transaction->rollback();
    watchdog_exception(WATCHDOG_ERROR, $e, t('Can no delete media crop settings!'));
    return FALSE;
  }
}

/**
 * @param $id
 * @return XMediaCrop
 */
function xmedia_crop_load($id) {
  $data = db_select('xmedia_crops', 'xc')
    ->fields('xc')
    ->condition('id', $id)
    ->execute()->fetch();

  $crop = new XMediaCrop();
  $crop->populateFromClass($data);
  return $crop;
}

/**
 * Get all crop settings for entity
 *
 * @param $entity_id
 * @return array
 */
function xmedia_crop_db_get_by_entity_id($entity_id) {
  $results = array();

  $crops = db_select('xmedia_crops', 'c')
    ->fields('c')
    ->condition('entity_id', $entity_id)
    ->execute()
    ->fetchAll();

  if (is_array($crops)) {
    foreach ($crops as $crop) {
      $xmedia_crop = new XMediaCrop();
      $xmedia_crop->populateFromClass($crop);
      $results[$crop->image_style] = $xmedia_crop;
    }
  }

  return $results;
}

/**
 * Get crop settings by entity ID and style name
 *
 * @param $entity_id
 * @param $style_name
 * @return XMediaCrop|NULL
 */
function xmedia_crop_db_get_by_entity_id_and_style($entity_id, $style_name) {
  $crops = db_select('xmedia_crops', 'c')
    ->fields('c')
    ->condition('entity_id', $entity_id)
    ->condition('image_style', $style_name)
    ->execute()
    ->fetchAll();
  $crop = array_shift($crops);

  if ($crop === NULL) {
    return NULL;
  }

  $xmedia_crop = new XMediaCrop();
  $xmedia_crop->populateFromClass($crop);
  return $xmedia_crop;
}