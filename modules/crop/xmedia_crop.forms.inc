<?php

function xmedia_crop_form_styles($form, &$form_state, XMediaEntity $xmedia) {
  
  $form = array();
  $styles = image_styles();

  $form_state['xmedia'] = $xmedia;
  
  $form['items'] = array(
    '#type' => 'vertical_tabs',
    '#tree' => TRUE
  );

  $form['styles'] = array(
    '#type' => 'vertical_tabs',
    '#tree' => TRUE,
  );
  
  $crops = xmedia_crop_db_get_by_entity_id($xmedia->id);
  foreach ($styles as $style) {
    
    $coords = xmedia_crop_get_default_coords_from_style($style);
    $name = $style['name'];
    
    $form['styles'][$name] = array(
      '#type' => 'fieldset',
      '#title' => $name,
      '#collapsible' => TRUE,
      '#group' => 'items',
      '#attributes' => array(
        'class' => array('crop-image-set'),
      ),
    );
    
    $file = file_load($xmedia->fid);
    $image_properties = array(
      'path' => $file->uri,
      'attributes' => array(
        'class' => array('jcrop-image'),
      ),
    );
    
        // ID
    $form['styles'][$name]['id'] = array(
      '#type' => 'hidden',
      '#value' => key_exists($name, $crops) ? $crops[$name]->id : '',
    );
    
        // Save
    $form['styles'][$name]['save'] = array(
      '#type' => 'checkbox',
      '#title' => t('Override crop settings?'),
      '#default_value' => key_exists($name, $crops) ? 1 : 0,
      '#attributes' => array(
        'class' => array('save'),
      ),
    );
    
        // X
    $form['styles'][$name]['x'] = array(
      '#type' => 'hidden',
      '#title' => 'X',
      '#size' => 15,
      '#maxlength' => 15,
      '#attributes' => array(
        'class' => array('jcrop-x'),
      ),
      '#default_value' => key_exists($name, $crops) ? $crops[$name]->x : $coords['x']
    );
    
        // Y
    $form['styles'][$name]['y'] = array(
      '#type' => 'hidden',
      '#title' => 'Y',
      '#size' => 15,
      '#maxlength' => 15,
      '#attributes' => array(
        'class' => array('jcrop-y'),
      ),
      '#default_value' => key_exists($name, $crops) ? $crops[$name]->y : $coords['y']
    );

    // X1
    $form['styles'][$name]['x1'] = array(
      '#type' => 'hidden',
      '#title' => 'X1',
      '#size' => 15,
      '#maxlength' => 15,
      '#attributes' => array(
        'class' => array('jcrop-x1'),
      ),
      '#default_value' => key_exists($name, $crops) ? $crops[$name]->x1 : $coords['x1']
    );

    // Y1
    $form['styles'][$name]['y1'] = array(
      '#type' => 'hidden',
      '#title' => 'Y1',
      '#size' => 15,
      '#maxlength' => 15,
      '#attributes' => array(
        'class' => array('jcrop-y1'),
      ),
      '#default_value' => key_exists($name, $crops) ? $crops[$name]->y1 : $coords['y1']
    );
    
        // Height
    $form['styles'][$name]['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#size' => 15,
      '#maxlength' => 15,
      '#attributes' => array(
        'class' => array('jcrop-height'),
      ),
      '#default_value' => key_exists($name, $crops) ? $crops[$name]->height : $coords['h']
    );

    // Width
    $form['styles'][$name]['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#size' => 15,
      '#maxlength' => 15,
      '#attributes' => array(
        'class' => array('jcrop-width'),
      ),
      '#default_value' => key_exists($name, $crops) ? $crops[$name]->width : $coords['w']
    );
    
        // Min height
    $form['styles'][$name]['min-height'] = array(
      '#type' => 'hidden',
      '#value' => $coords['h'],
      '#attributes' => array(
        'class' => array('jcrop-min-height'),
      ),
    );

    // Min width
    $form['styles'][$name]['min-width'] = array(
      '#type' => 'hidden',
      '#value' => $coords['w'],
      '#attributes' => array(
        'class' => array('jcrop-min-width'),
      ),
    );
    
    $form['styles'][$name]['image'] = array(
      '#type' => 'item',
      '#markup' => theme('image', $image_properties),
    );
    
  }
  
    $form['#attached'] = array(
    'js' => array(drupal_get_path('module', 'xmedia_crop') . '/js/xmedia_crop_summary.js'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

function xmedia_crop_form_styles_submit($form, &$form_state) {
  $styles = $form_state['values']['styles'];
  $xmedia = $form_state['xmedia'];

  foreach ($styles as $style_name => $c) {
    $c['origin_image_style'] = $style_name;
    $c['entity_id'] = $xmedia->id;

    $xmedia_crop = new XMediaCrop();
    $xmedia_crop->populateFromArray($c);

    // Save crop settings
    if (!empty($c['save']) && $c['save'] == 1) {
      // Check if user filled all mandatory fields
      if (isset($c['x']) && isset($c['y']) && !empty($c['x1']) && !empty($c['y1']) && !empty($c['height']) && !empty($c['width'])) {
        if (!xmedia_crop_db_save($xmedia_crop)) {
          drupal_set_message(t('Can not save media coordinates!'), 'error');
        }
      }
      // Remove crop settings
    }
    else {
      if (!xmedia_crop_db_delete($xmedia_crop)) {
        drupal_set_message(t('Can not delete media coordinates'), 'error');
      }
    }

  }
}



function xmedia_crop_form_style($xmedia, $image_style = NULL) {

  $file = file_load($xmedia->fid);
  $image_properties = array(
    'path' => $file->uri,
    'attributes' => array(
      'class' => array('jcrop-image'),
    ),
    'style_name' => $image_style,
    'width' => '',
    'height' => '',
  );
    
  $image_info = image_get_info(drupal_realpath($file->uri));
  if($image_info) {
    $image_properties['width'] = $image_info['width'];
    $image_properties['height'] = $image_info['height'];
  }

  $form['styles']['set']['image'] = array(
    '#type' => 'item',
    '#attributes' => array('id' => 'supertest'),
    '#markup' => empty($image_style) ? theme_image($image_properties) : theme_image_style($image_properties),
  );
  return $form;
}

function xmedia_crop_form_xmedia_ui_insert_form_alter(&$form, &$form_state, $xmedia) {
  drupal_add_library('xmedia_crop', 'xmedia_crop');
  
  $xmedia = $form['#xmedia'];
  $xmedia_image = field_get_items('xmedia', $xmedia, 'xmedia_image');
  $setting_crop_from_preset = variable_get('xmedia_crop_wysiwyg_preset_base', 'large');
  
  $image_styles = image_styles();
  $setting_crop_available_styles_default = array();
  foreach($image_styles as $preset => $image_style) {
    $setting_crop_available_styles_default[] = $preset;
  }
  $setting_crop_available_styles = variable_get('xmedia_crop_wysiwyg_available_styles', $setting_crop_available_styles_default);
  $setting_crop_from_style = variable_get('xmedia_crop_wysiwyg_style_origin', 'large');
  
  $settings_js_styles = array();
  foreach($setting_crop_available_styles as $style) {
    $settings_js_styles[$style] = xmedia_crop_get_default_coords_from_style($image_styles[$style]);
  }
  
  drupal_add_js(array('xmediaCrop' => array('styles' => $settings_js_styles)), 'setting');
  
  $image_style_options = array('');
  foreach($setting_crop_available_styles as $style) {
    $image_style_options[$style] = $style;
  }
  $form['image_info']['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => $image_style_options,
    '#weight' => 50,
  );
  
  $form['image_info']['lock_aspect_ratio'] = array(
    '#title' => t('Lock aspect ratio'),
    '#type' => 'checkbox',
    '#default_value' => TRUE,
    '#weight' => 51,
  );
  
  $form['image_crop'] = array(
    '#type' => 'fieldset',
    '#title' => t('Crop view'),
    '#collapsible' => FALSE,
  );
  
  $xmedia_image_markup = field_view_value('xmedia', $xmedia, 'xmedia_image', $xmedia_image[0], array(
    'type' => 'image',
    'settings' => array(
      'image_style' => $setting_crop_from_style,
      'image_link' => 'none',
    ),
  ));
  
  $form['image_crop']['crop_area'] = array(
    '#type' => 'item',
    '#markup' => drupal_render($xmedia_image_markup),
  );
  
  $form['crop_style_origin'] = array(
    '#type' => 'hidden',
    '#value' => $setting_crop_from_style,
  );
  
  $form['crop_coords'] = array(
    '#type' => 'hidden',
    '#value' => '',
  );
  
}