<?php

function xmedia_crop_styles($xmedia) {
  drupal_add_library('xmedia_crop', 'xmedia_crop');

  return theme('xmedia_crop_styles', array(
    'form' => drupal_get_form('xmedia_crop_form_styles', $xmedia),
  ));
}