<?php


function xmedia_crop_image(stdClass $image, $settings) {
  $style_name = xmedia_crop_get_style_name_from_uri();
  $fid = xmedia_crop_get_fid_by_uri($image->source);
  $xmedia = xmedia_db_get_media_by_fid($fid);
  $crop = xmedia_crop_db_get_by_entity_id_and_style($xmedia->id, $style_name);

  // No crop setting found
  if ($crop == NULL) {
    return FALSE;
  }

  image_crop($image, $crop->x, $crop->y, $crop->width, $crop->height);
}

/**
 * Get image fid by uri
 *
 * @param string $uri
 * @return int
 */
function xmedia_crop_get_fid_by_uri($uri) {
  $images = db_select('file_managed', 'f')
    ->fields('f')
    ->condition('uri', $uri)
    ->execute()
    ->fetchAll();

  $image = array_shift($images);
  return $image->fid;
}

/**
 * Get style name from URI
 *
 * @return bool|string
 */
function xmedia_crop_get_style_name_from_uri() {
  $split = explode('/', $_GET['q']);
  $pointer = array_search('styles', $split);

  if ($pointer !== FALSE) {
    return $split[++$pointer];
  }

  return FALSE;
}

/**
 * Get best coords by style's effects
 *
 * @param array $style
 * @return array
 */
function xmedia_crop_get_default_coords_from_style($style = array()) {
  $coords = array();

  $coords['x'] = 0;
  $coords['y'] = 0;

  $coords['x1'] = '';
  $coords['y1'] = '';
  $coords['w'] = '';
  $coords['h'] = '';


  // Search for crop
  foreach ($style['effects'] as $effect) {
    if ($effect['name'] == 'image_crop') {
      $coords['x1'] = $effect['data']['width'];
      $coords['y1'] = $effect['data']['height'];
      $coords['w'] = $effect['data']['width'];
      $coords['h'] = $effect['data']['height'];
    }
  }

  if ($coords['x1'] == '' || $coords['y1'] == '') {
    foreach ($style['effects'] as $effect) {
      if (isset($effect['data']['width'])) {
        $coords['x1'] = $effect['data']['width'];
        $coords['y1'] = $effect['data']['height'];
        $coords['h'] = $effect['data']['width'];
        $coords['w'] = $effect['data']['height'];
      }
    }
  }

  return $coords;
}