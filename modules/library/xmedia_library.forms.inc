<?php

function xmedia_library_list_form($form, &$form_state) {
  $form = array();

  // Hook implementation
  $options = array(
    'xmedia_library_action_remove_selected' => t('Remove selected media')
  );
  $options = array_merge(module_invoke_all('xmedia_actions_options'), $options);

  // Class
  $form['#attributes']['class'] = array('library-form');

  // Search submit
  $form['search_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search Media'),
    '#submit' => array('xmedia_library_list_form_search_submit'),
    '#attributes' => array(
      'class' => array('library-search-submit'),
    ),
  );

  // Search
  $search = !empty($_GET['search']) ? $_GET['search'] : NULL;
  $form['search'] = array(
    '#type' => 'textfield',
    '#default_value' => $search,
    '#attributes' => array(
      'class' => array('library-search-input'),
    ),
  );

  // Actions
  $form['action'] = array(
    '#title' => t('Action:'),
    '#type' => 'select',
    '#options' => $options,
    '#empty_value' => '',
    '#empty_option' => '-------',
  );

  // Actions submit
  $form['action_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
    '#submit' => array('xmedia_library_list_form_actions_submit'),
  );

  $medias = xmedia_db_get_media_list($search);

  if (is_array($medias)) {
    foreach ($medias as $key => $media) {
      $file = file_load($media->fid);
      $xmedia = new XMedia();
      $xmedia->populateFromObj($media);

      $image_properties = array(
        'path' => $file->uri,
        'style_name' => XMEDIA_LIBRARY_IMAGE_STYLE,
      );

      $image = theme('image_style', $image_properties);

      $form['items'][$media->id] = array(
        '#type' => 'checkbox',
        '#tree' => TRUE,
        '#params' => array(
          'image' => $image,
          'title' => $xmedia->title,
          'author' => $xmedia->getAuthorName(),
          'type' => $xmedia->type,
          'filetype' => $xmedia->getFiletype(),
          'filesize' => $xmedia->getFilesize(),
          'date' => format_date($xmedia->created),
          'actions' => xmedia_library_actions($xmedia),
        )
      );
    }
  }

  return $form;
}

function xmedia_library_list_form_search_submit($form, &$form_state) {
  $search = $form_state['values']['search'];

  $form_state['redirect'] = array(
    current_path(), array(
      'query' => array(
        'search' => $search,
      ),
    ),
  );
}

function xmedia_library_list_form_actions_submit($form, &$form_state) {
  $data = $form_state['values'];

  // Get list of selected media
  $selected_media = array();
  foreach ($data as $data_key => $data_value) {
    if (is_numeric($data_key) && is_numeric($data_value) && $data_value == 1) {
      $selected_media[] = $data_key;
    }
  }

  // Call user defined function if exists
  if (!empty($data['action'])) {
    if (function_exists($data['action'])) {
      call_user_func($data['action'], $selected_media);
    }
    else {
      drupal_set_message(t('Function with name "@name" does not exist!', array(
        '@name' => $data['action'],
      )), 'warning');
    }

  }
}

/**
 * Bulk upload form
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function xmedia_library_form_upload($form, &$form_state) {
  $items = array();
  $library = drupal_get_library('plupload', 'plupload');

  if(!file_exists(_plupload_library_path())) {
    drupal_set_message(t('System can not find plupload library at path !path. Please,
      download the library !here and place it to desired location',
      array(
        '!path' => _plupload_library_path(),
        '!here' => l('here', $library['website']))),
      'warning'
    );
  }

  $items['type'] = array(
    '#type' => 'select',
    '#options' => xmedia_db_get_bundles_keyed(),
  );

  $items['files'] = array(
    '#type' => 'plupload',
    '#title' => t('Files'),
  );

  $items['bulk_update'] = array(
    '#type' => 'checkbox',
    '#title' => t('I want to edit medias after upload'),
  );

  $items['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array('xmedia_library_form_upload_submit'),
  );

  return $items;
}

/**
 * Submit bulk upload
 * @param $form
 * @param $form_state
 */
function xmedia_library_form_upload_submit($form, &$form_state) {
  $scheme = variable_get('file_default_scheme', 'public') . '://';
  $saved_files = array();
  $ids = array();

  // We can't use file_save_upload() because of http://www.jacobsingh.name/content/tight-coupling-no-not
  foreach ($form_state['values']['files'] as $uploaded_file) {
    if ($uploaded_file['status'] == 'done') {
      $source = $uploaded_file['tmppath'];

      // TODO: 'xmedia/' path - Use the path info from the xmedia type image field
      $destination = file_stream_wrapper_uri_normalize($scheme . 'xmedia/' . $uploaded_file['name']);
      $destination = file_unmanaged_move($source, $destination, FILE_EXISTS_RENAME);

      if ($destination) {
        $file = plupload_file_uri_to_object($destination);

        $result = xmedia_db_save_media_from_file($file, $form_state['values']['type']);

        if ($result) {
          $ids[] = $result->id;
        }

        $saved_files[] = $file;
      }
      else {
        drupal_set_message(t('Unable to save file. Check your !settings.'), array(
          '!settings' =>
          l(t('filesystem configuration'), 'admin/config/media/file-system')
        ), FALSE);
      }

    }
    else {
      form_set_error('pud', "Upload of {$uploaded_file['name']} failed");
    }
  }

  // query is not empty so we want to make bulk update
  if (sizeof($ids) && $form_state['values']['bulk_update']) {
    $id = array_shift($ids);
    $form_state['redirect'] = array(
      'admin/structure/xmedia-library/update/' . $id, array(
        'query' => array(
          'ids' => $ids,
        )
      )
    );
  }
  else {
    $form_state['redirect'] = variable_get('xmedia_library_path', 'admin/structure/xmedia-library');
  }

}


/**
 * Bulk update form
 * @param $form
 * @param $form_state
 * @param $xmedia
 * @return array
 */
function xmedia_library_form_bulk_update($form, &$form_state, $xmedia) {
  drupal_set_title(xmedia_page_edit_title($xmedia));

  $form = xmedia_form_media($form, $form_state, $xmedia);

  $form['bulk_update_query'] = array(
    '#type' => 'value',
    '#value' => 'x',
  );

  // for all buttons there is same redirect
  $form['actions']['submit']['#submit'][] = 'xmedia_library_form_bulk_update_submit';
  $form['actions']['delete']['#submit'][] = 'xmedia_library_form_bulk_update_submit';
  $form['actions']['cancel']['#submit'][] = 'xmedia_library_form_bulk_update_submit';

  // cancel all button will stop bulk update
  $form['actions']['cancel_all'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel all'),
    '#submit' => array('xmedia_library_form_bulk_update_cancel'),
    '#weight' => 10,
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 */
function xmedia_library_form_bulk_update_submit($form, &$form_state) {
  $params = drupal_get_query_parameters();

  $ids = array();
  if (isset($params['ids'])) {
    $ids = $params['ids'];
  }

  if (sizeof($ids)) {
    $id = array_shift($ids);
    $form_state['redirect'] = array(
      'admin/structure/xmedia-library/update/' . $id, array(
        'query' => array(
          'ids' => $ids,
        )
      )
    );
  }
  else {
    $form_state['redirect'] = variable_get('xmedia_library_path', 'admin/structure/xmedia-library');
  }
}

/**
 * Cancel whole bulk update
 *
 * @param $form
 * @param $form_state
 */
function xmedia_library_form_bulk_update_cancel($form, &$form_state) {
  $form_state['redirect'] = variable_get('xmedia_library_path', 'admin/structure/xmedia-library');
}

