<?php
function xmedia_library() {
  drupal_add_library('xmedia_library', 'xmedia_library');
  return drupal_render(drupal_get_form('xmedia_library_list_form'));
}

/**
 * Display library links
 *
 * @param XMedia $xmedia
 * @return string
 */
function xmedia_library_actions($xmedia) {
  $links = array(
    array(
      'title' => t('Update'),
      'href' => 'admin/structure/xmedia-library/update/' . $xmedia->id,
    ),
    array(
      'title' => t('Remove'),
      'href' => 'admin/library/remove/' . $xmedia->id,
    ),
  );

  // invoke hook to collect actions from other modules
  $links = array_merge(module_invoke_all('xmedia_actions', $xmedia), $links);

  return theme('links', array(
    'links' => $links,
  ));
}

/**
 * Uploads new media objects
 *
 * @return array|mixed
 */
function xmedia_library_upload() {
  return drupal_get_form('xmedia_library_form_upload');
}

/**
 * Page callback
 * uri: admin/library/remove/%media
 *
 * @param array $xmedias
 */
function xmedia_library_remove($xmedias = array()) {

  if ($xmedias == NULL) {
    drupal_set_message(t('No xmedia file found'));
    drupal_goto('admin/library');
  }

  // Multiple media files to remove
  if (!is_array($xmedias)) {
    $xmedias = array($xmedias->id => $xmedias);
  }

  foreach ($xmedias as $xmedia) {
    if (xmedia_delete($xmedia) === FALSE) {

      drupal_set_message(t('Can not remove file "%title".', array(
        '%title' => $xmedia->title,
      )), 'error');
    }
    else {

      drupal_set_message(t('Media file "%title" successfully removed.', array(
        '%title' => $xmedia->title,
      )));
    }
  }

  drupal_goto('admin/library');
}

/**
 * Page callback
 * uri: admin/library/update/%xmedia
 *
 * @param XMedia $xmedia
 * @return string
 */
function xmedia_library_update($xmedia) {

  // add library assets
  drupal_add_library('xmedia_library', 'xmedia_library');

  return theme('xmedia_library_detail', array(
    'xmedia' => $xmedia
  )) . drupal_render(drupal_get_form('xmedia_form_media', $xmedia));
}