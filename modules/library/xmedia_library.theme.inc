<?php

/**
 * Implements HOOK_theme()
 * @return array
 */
function xmedia_library_theme() {
  return array(
    'xmedia_library_list_form' => array(
      'render element' => 'form'
    ),
    'xmedia_library_detail' => array(
      'variables' => array('xmedia' => NULL),
      'template' => 'theme/xmedia-library-detail',
    )
  );
}

/**
 * Preprocess function for: xmedia-library-detail.tpl.php
 */
function template_preprocess_xmedia_library_detail(&$variables) {
  $xmedia = $variables['xmedia'];
  $file = file_load($xmedia->fid);

  $image_properties = array(
    'path' => $file->uri,
    'style_name' => 'xmedia_detail',
  );

  $image = theme('image_style', $image_properties);

  $variables['xmedia'] = $xmedia;
  $variables['image'] = $image;
  $variables['file'] = $file;
}

function theme_xmedia_library_list_form($variables) {
  $form = $variables['form'];
  $header = xmedia_library_list_header();

  // Data
  if (array_key_exists('items', $form) && is_array($form['items'])) {
    $items = array();

    foreach (element_children($form['items']) as $item) {
      if (is_array($form['items'][$item])) {
        $row = array(
          'data' => array(
            render($form['items'][$item]),
            $form['items'][$item]['#params']['image'],
            l($form['items'][$item]['#params']['title'], 'admin/library/update/' . $item),
            $form['items'][$item]['#params']['author'],
            $form['items'][$item]['#params']['type'],
            $form['items'][$item]['#params']['filetype'],
            $form['items'][$item]['#params']['filesize'],
            $form['items'][$item]['#params']['date'],
            $form['items'][$item]['#params']['actions'],
          )
        );

        $items[] = $row;
      }
    }
  }

  // Table
  $variables = array(
    'header' => $header,
    'rows' => isset($items) ? $items : array(),
    'empty' => t('No media files found.'),
    'attributes' => array(
      'class' => array('library'),
    ),
  );

  // Render
  $output = drupal_render_children($form);
  $output .= theme('table', $variables) . theme('pager', array('parameters' => array('limit' => XMEDIA_PAGER_SIZE)));

  return $output;
}