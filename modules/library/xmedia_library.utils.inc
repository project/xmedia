<?php

/**
 * Table form action callback for removing multiple media.
 *
 * @param array $media
 * @return void
 */
function xmedia_library_action_remove_selected($media = array()) {
  // No files were selected
  if (sizeof($media) == 0) {
    return;
  }

  // Create URL param
  $ids = implode('+', $media);
  drupal_goto('admin/library/remove/' . $ids);
}

/**
 * Gets library table header
 *
 * @return array
 */
function xmedia_library_list_header() {
  $header = array(
    array('data' => '<input type="checkbox" value="all">', 'class' => 'all'),
    array('data' => t('Preview'), 'class' => 'image'),
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('Author'), 'field' => 'user_id'),
    array('data' => t('Bundle'), 'field' => 'type'),
    array('data' => t('Type'), 'class' => 'type'),
    array('data' => t('Size'), 'class' => 'size'),
    array('data' => t('Date'), 'field' => 'created', 'class' => array('date')),
    array('data' => t('Actions'), 'class' => array('actions')),
  );

  return $header;
}