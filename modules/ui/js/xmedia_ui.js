function hideXmediaItem(context) {

  var checkbox = jQuery('input', context);

  var item = checkbox.parent().parent().next().children();

  if (checkbox.is(':checked')) {
    item.slideUp('fast');
  } else {
    item.slideDown('fast');
  }
}

Drupal.behaviors.xmediaBrowser = {

  attach:function (context, settings) {

    jQuery('.xmedia-remove .form-type-checkbox').once('close', function () {
      var context = jQuery(this);
      context.click(function () {

        var checkbox = jQuery('input', this);
        if (checkbox.is(':checked')) {
          context.removeClass('xmedia-restore');
          checkbox.attr('checked', false);
        } else {
          context.addClass('xmedia-restore');
          checkbox.attr('checked', true);
        }

        hideXmediaItem(context);
      });

    });

    jQuery('.xmedia-remove .form-type-checkbox').each(function (index) {
      var context = jQuery(this);
      var checkbox = jQuery('input', this);

      if (checkbox.is(':checked')) {
        context.addClass('xmedia-restore');
      } else {
        context.removeClass('xmedia-restore');
      }
      hideXmediaItem(context);
    });

    jQuery('.xmedia-browser-modal').fancybox({
      'width': 980,
      'height': 940,
      'titleShow': false,
      'autoscale': false,
      'autoDimensions': false,
      'scrolling': 'no',
      'type':'iframe',
      'onComplete': function() {
        jQuery('#fancybox-frame').load(function () {
          jQuery('#fancybox-inner').height(jQuery(this).contents().find('#xmedia-browser').height() + 70);
          jQuery('#fancybox-wrap').height(jQuery(this).contents().find('#xmedia-browser').height() + 100);
          return true;
        })
      }
    });


  }
}




