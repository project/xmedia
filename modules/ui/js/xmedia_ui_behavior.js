Drupal.behaviors.xmediaBrowserBehavior = {
  attach:function (context, settings) {
    if(Drupal.settings.xmediaBrowser.mode == 'widget') {
      jQuery('.xmedia-selectable:not(.xmedia-processed)')
        .addClass('xmedia-processed')
        .bind('click', function () {
          var item = jQuery(this);
          if (item.hasClass('xmedia-not-selected')) {
            if(Drupal.settings.xmediaBrowser.limit == 1) {
              jQuery('.xmedia-selectable').removeClass('xmedia-selected');
              jQuery('.xmedia-selectable').addClass('xmedia-not-selected');
            }
            item.removeClass('xmedia-not-selected');
            item.addClass('xmedia-selected');
          } else {
            item.addClass('xmedia-not-selected');
            item.removeClass('xmedia-selected');
          }
        });
      
      //buttons events for field widget browser
      jQuery('#xmedia-add-images:not(.xmedia-processed)')
        .addClass('xmedia-processed')
        .bind('click', function () {
          var str = '';
          var count = 0;
          var field_id = Drupal.settings.xmediaBrowser.field;
          var field_count = 0;
          var limit = Drupal.settings.xmediaBrowser.limit;

          jQuery(document).find('.xmedia-selected').each(function (key, element) {
            jQuery(element).attr('id');
            str += jQuery(element).attr('id').toString() + ",";
            count++;
          });

          window.parent.jQuery('input[name^="' + field_id + '[und]"]').each(function(){
            var input_name = jQuery(this).attr('name');
            if(jQuery(this).is('input[name$="[mid]"]') && window.parent.jQuery('input[name="' + input_name.replace('[mid]','[_remove]') + '"]').is(':checked') == false) {
              field_count++;
            }
          });
          if(limit > 0 && (field_count + count) > limit) {
            alert(Drupal.t('You are reach the number of images that can be added to this field.'));
            return false;
          }

          window.parent.jQuery('#' + field_id + '_mid_elements').attr('value', str);
          window.parent.jQuery('#' + field_id + '_add_more').trigger('mousedown');
          if(limit == 1) {
            window.parent.jQuery.fancybox.close();
          }
          return false;
        });

      jQuery('#xmedia-cancel-images').click(function () {
        window.parent.jQuery.fancybox.close();
      });
    } else if(Drupal.settings.xmediaBrowser.mode == 'wizard') {
      jQuery('.xmedia-selectable:not(.xmedia-processed)')
        .addClass('xmedia-processed')
        .bind('click', function () {
          var item = jQuery(this);
          if (item.hasClass('xmedia-not-selected')) {
            jQuery('.xmedia-selectable').removeClass('xmedia-selected');
            jQuery('.xmedia-selectable').addClass('xmedia-not-selected');
            item.removeClass('xmedia-not-selected');
            item.addClass('xmedia-selected');
            Drupal.settings.xmediaBrowser.selectedImage = item.attr('id');
          } else {
            jQuery('.xmedia-selectable').removeClass('xmedia-selected');
            jQuery('.xmedia-selectable').addClass('xmedia-not-selected');
            Drupal.settings.xmediaBrowser.selectedImage = null;
          }
        });
    }
  }
}