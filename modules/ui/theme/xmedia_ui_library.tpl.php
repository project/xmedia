<table>
  <?php foreach ($rows as $items): ?>
  <tr>
    <?php foreach ($items as $item): ?>
    <td class="xmedia-library-item">
      <div id="<?php print $item['entity']->id; ?>" class="xmedia-selectable xmedia-not-selected">
        <?php print $item['content']; ?>
      </div>
    </td>
    <?php endforeach; ?>
  </tr>
  <?php endforeach; ?>
</table>

<a class="button form-submit" id="xmedia-add-images"><?php print t('Add images'); ?></a>
<a class="button form-submit" id="xmedia-cancel-images"><?php print t('Close'); ?></a>
