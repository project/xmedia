<div id="xmedia-ui-library" style="width:940px;">
  <div id="xmedia-ui-library-wrapper">
    <div id="xmedia-ui-library-content">
      <div id="xmedia-ui-library-content-wrapper">

        <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
        <?php endif; ?>

        <div id="xmedia-ui-library-list-wrapper">
          <?php
          print $list;
          ?>
        </div>
      </div>
    </div>
  </div>
</div>