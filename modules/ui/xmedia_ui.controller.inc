<?php
/**
 * Hooks:
 *  - entity_load_all($xmedias)
 *  - entity_load_one($xmedia)
 *  - entity_load_multiple($xmedias)
 *  - entity_save($xmedias)
 *  - entity_update($xmedias)
 *  - entity_remove($xmedia)
 */
class XMediaDisplayController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'alternate_text' => '',
      'caption' => '',
      'crop_id' => '',
      'meta_dimensions' => '0x0',
      'created' => REQUEST_TIME,
      'modified' => REQUEST_TIME,
      'user_id' => $user->uid,
    );

    return parent::create($values);
  }

}