<?php


/**
 * @param $type
 * @return array|mixed
 */
function xmedia_display_type_load($type) {
  return xmedia_display_get_bundles($type);
}



function xmedia_display_load($ids) {

  if(is_array($ids)) {
    return xmedia_display_load_multiple($ids);
  }
  
  $xmedia_displays = xmedia_display_load_multiple(array($ids));

  return array_shift($xmedia_displays);
}

/**
 * @param $ids
 * @return mixed
 */
function xmedia_display_load_multiple($ids) {
  return entity_load('xmedia_display', $ids);
}

/**
 * @param $media
 * @return bool
 */
function xmedia_display_delete($media) {
  return entity_delete('xmedia_display', entity_id('xmedia_display', $media));
}

/**
 * @param $media
 * @return bool
 */
function xmedia_display_type_delete($media) {
  return entity_delete('xmedia_display_type', entity_id('xmedia_display_type', $media));
}

/**
 * @param array $ids
 * @return bool
 */
function xmedia_display_delete_multiple(array $ids) {
  return entity_delete_multiple('xmedia_display', $ids);
}

/**
 * @param array $ids
 * @return bool
 */
function xmedia_display_type_delete_multiple(array $ids) {
  return entity_delete_multiple('xmedia_display_type', $ids);
}


/**
 * Save XMedia entity
 *
 * @param XMedia $media
 * @return mixed
 */
function xmedia_display_save($media) {
  return entity_save('xmedia_display', $media);
}

/**
 * Save XMediaType entity
 *
 * @param $media_type
 * @return mixed
 */
function xmedia_display_type_save($media_type) {
  return entity_save('xmedia_type', $media_type);
}

/**
 * Get media by id or array of ids
 *
 * @param $ids
 * @return void
 */
function xmedia_ui_db_get_media($ids) {
  $formed_ids = array();
  if (is_array($ids)) {
    foreach ($ids as $id) {
      $formed_ids[$id] = $id;
    }
  }
  else {
    $formed_ids[$ids] = $ids;
  }

  return entity_get_controller('xmedia_display')->load($formed_ids);
}

/**
 * Remove media
 *
 * @param $media
 * @return
 */
function xmedia_ui_db_remove_media($media) {
  return entity_get_controller('xmedia_display')->remove($media);
}

/**
 * Get all media files in library
 *
 * @return array
 */
function xmedia_ui_db_get_all_media() {
  return entity_get_controller('xmedia_display')->loadAll();
}

/**
 * @param $media
 * @return mixed
 */
function xmedia_ui_db_save_media($media) {
  return entity_get_controller('xmedia_display')->save($media);
}

/**
 * @param $type_name
 * @return array|mixed
 */
function xmedia_display_get_bundles($type_name = NULL) {
  $types = entity_load_multiple_by_name('xmedia_display_type', isset($type_name) ? array($type_name) : FALSE);
  if(isset($type_name)) {
    return isset($types[$type_name]) ? $types[$type_name] : FALSE;
  } else {
    return $types;
  }  
}