<?php

/**
 * Implements hook_element_info()
 */
function xmedia_ui_element_info() {
  $types = array();

  $types['xmedia'] = array(
    '#input' => TRUE,
    '#process' => array('xmedia_element_process'),
    '#element_validate' => array(),
    '#required' => FALSE,
  );

  return $types;
}

/**
 * Element process
 *
 * @param $element
 * @param $form_state
 * @param $form
 * @return array
 */
function xmedia_element_process(&$element, &$form_state, &$form) {

  $item = $element['#item'];

  $delta = $element['#delta'];

  $language = $element['#language'];
  $field_name = $element['#field_name'];

  $field = field_info_field($element['#field_name']);
  $field_instance_settings = $field['settings'];
  
  if(empty($field_instance_settings['xmedia_display_bundle']) || !xmedia_display_get_bundles($field_instance_settings['xmedia_display_bundle'])) {
    drupal_set_message(t('There is missing Xmedia Display bundle on field name %field.', array('%field' => $element['#field_name'])), 'error');
    return $element;
  }

  $xmedia_display = entity_create('xmedia_display', array('type' => $field_instance_settings['xmedia_display_bundle']));
  $data = array();

  $data['xmedia_display'] = $xmedia_display;

  $mid = NULL;
  if (isset($form_state['values'][$field_name][$language][$delta]['mid'])) {
    $mid = $form_state['values'][$field_name][$language][$delta]['mid'];
  }

  // load source xmedia if mid available
  if (!empty($mid) || !empty($item['mid'])) {
    $mid = !empty($item['mid']) ? $item['mid'] : $mid;
    $xmedia = xmedia_load($mid);

    if (isset($item['display_mid']) && !empty($item['display_mid'])) {
      $xmedia_display = xmedia_display_load($item['display_mid']);
    }

    $data = array(
      'display' => $xmedia,
      'xmedia_display' => $xmedia_display,
    );
  }

  // Store our entities data
  $element['entity'] = array(
    '#type' => 'value',
    '#value' => $data,
  );


  // Display empty message if no media
  if (empty($mid)) {
    $element['no_media_message'] = array(
      '#markup' => '<div class=xmedia-message>' . t('No media file choosen') . '</div>',
    );
  }

  if (!empty($mid) && $xmedia_display) {
    // Display image thumbnail.
    // This thumbnail is by no way connected with resulting xmedia display object
    $fid = !empty($item['fid']) ? $item['fid'] : $xmedia->fid;

    $element['fid'] = array(
      '#type' => 'value',
      '#value' => $fid,
    );

    $element['add_fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Additional info'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#weight' => 50,
    );

    // Set #parents to 'top-level' by default.
    $form += array('#parents' => array($delta));

    // If no language is provided use the default site language.

    $options = array('language' => field_valid_language(LANGUAGE_NONE));
    $add_fields = (array) _field_invoke_default('form', 'xmedia_display', $xmedia_display, $element, $form_state, $options);
    $element['add_fields'] += (array) _field_invoke_default('form', 'xmedia_display', $xmedia_display, $element, $form_state, $options);

    //Prefill default values from xmedia entity
    $xmedia_properties = get_object_vars($xmedia);

    foreach (array_keys($add_fields) as $field_id) {
      if (!empty($xmedia_properties[$field_id]['und'][0]['value']) && empty($element['add_fields'][$field_id]['und'][0]['value']['#default_value'])) {
        $element['add_fields'][$field_id]['und'][0]['value']['#default_value'] = $xmedia_properties[$field_id]['und'][0]['value'];
      }
    }


    // Add custom weight handling.
    list($id, $vid, $bundle) = entity_extract_ids('form', 'xmedia_display');

    $element['add_fields']['#pre_render'][] = '_field_extra_fields_pre_render';
    $element['add_fields']['#entity_type'] = 'xmedia_display';
    $element['add_fields']['#bundle'] = 'display';

    if (count($add_fields) <= 1) {
      $element['add_fields']['#attributes']['class'][] = 'hidden';
    }

    $element['image_info'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#title' => t('Image info'),
      '#attributes' => array('class' => array('xmedia-image-info'))
    );

    $element['mid'] = array(
      '#type' => 'hidden',
      '#title' => t('Title'),
      '#default_value' => $mid,
    );

    // Display mid is always zero
    // We don't want to create actual xmedia_display entities until node is submitted
    // Field is updated on entity presave action and all required references are created
    $element['display_mid'] = array(
      '#type' => 'value',
      '#value' => !empty($item['display_mid']) ? $item['display_mid'] : 0,
    );

    $element['image_info']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#default_value' => !empty($xmedia_display->title) ? $xmedia_display->title : $xmedia->title,
    );

    $element['image_info']['alternate_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Alternate text'),
      '#default_value' => !empty($xmedia_display->alternate_text) ? $xmedia_display->alternate_text : $xmedia->alternate_text,
    );

    $element['image_info']['caption'] = array(
      '#type' => 'textfield',
      '#title' => t('Caption'),
      '#default_value' => !empty($xmedia_display->caption) ? $xmedia_display->caption : $xmedia->caption,
    );

    $file = file_load($xmedia->fid);
    
    $image_properties = array(
      'path' => $file->uri,
      'attributes' => array(
        'class' => array('jcrop-image'),
      ),
    );
    
    $image_info = image_get_info(drupal_realpath($file->uri));
    if($image_info) {
      $image_properties['width'] = $image_info['width'];
      $image_properties['height'] = $image_info['height'];
    }

    if (!empty($crops->origin_image_style)) {
      $image_properties['style_name'] = $crops->origin_image_style;
    }
    else {
      $image_properties['style_name'] = $field_instance_settings['xmedia_default_crop_style'];
    }

    // Displa crop functionality only when crop module is enabled
    if (module_exists('xmedia_crop')) {
      if ($xmedia_display && !empty($xmedia_display->id)) {
        $saved_crops = xmedia_crop_db_get_by_entity_id($xmedia_display->id);
      }

      $image_styles = array_filter($field_instance_settings['xmedia_image_styles_to_crop']);

      $crop_wrapper_id = $field_name . '-' . $language . '-' . $delta . '-crop';
      $crop_wrapper_set_id = $crop_wrapper_id . '-fieldset';

      $element['image_info']['origin_image_style'] = array(
        '#type' => 'hidden',
        '#default_value' => $field_instance_settings['xmedia_default_crop_style'],
      );

      $file = file_load($fid);
      $image = image_load($file->uri);
      
      $element['styles'] = array(
        '#type' => 'container',
      );

      $element['image_info']['image_style'] = array(
        '#title' => t('Image style'),
        '#type' => 'select',
        '#options' => $image_styles,
        '#ajax' => array(
          'callback' => 'xmedia_ui_crop',
          'wrapper' => $crop_wrapper_id,
          'method' => 'html',
        ),
        '#attributes' => array(
          'class' => array('image_style')
        ),
        '#default_value' => !empty($xmedia_display->style_name) ? $xmedia_display->style_name : key($image_styles),
      );

      if ($field_instance_settings['xmedia_lock_aspect_ratio']) {
        $element['image_info']['ratio'] = array(
          '#type' => 'hidden',
          '#title' => t('Lock aspect ratio'),
          '#attributes' => array(
            'class' => array('ratio')
          ),
          '#default_value' => $field_instance_settings['xmedia_lock_aspect_ratio'],
        );
      }
      else {

        $element['image_info']['ratio'] = array(
          '#type' => 'checkbox',
          '#title' => t('Lock aspect ratio'),
          '#ajax' => array(
            'callback' => 'xmedia_ui_crop',
            'wrapper' => $crop_wrapper_id,
            'method' => 'html',
          ),
          '#attributes' => array(
            'class' => array('ratio')
          ),
          '#default_value' => !empty($crops->ratio) ? $crops->ratio : $field_instance_settings['xmedia_lock_aspect_ratio'],
        );
      }
      $element['styles'] = array(
        '#type' => 'container',
      );

      $element['styles']['set'] = array(
        '#type' => 'fieldset',
        '#title' => t('Crop view'),
        '#id' => $crop_wrapper_set_id,
        '#attributes' => array('class' => array('crop-image-set')),
        '#collapsible' => FALSE,
      );

      $element['styles']['set']['id'] = array(
        '#type' => 'hidden',
        '#value' => !empty($crops->id) ? $crops->id : 0,
      );
      
      $default_crops = array(
        'x' => 0,
        'y' => 0,
        'x1' => $image->info['width'],
        'y1' => $image->info['height'],
        'w' => $image->info['width'],
        'h' => $image->info['height'],
      );
      if(!empty($element['image_info']['image_style']['#default_value'])) {
        $style = image_style_load($element['image_info']['image_style']['#default_value']);
        $default_crops = xmedia_crop_get_default_coords_from_style($style);
        if (isset($saved_crops[$element['image_info']['image_style']['#default_value']])) {
          $default_saved_crop = $saved_crops[$element['image_info']['image_style']['#default_value']];
          $default_crops = array(
            'x' => $default_saved_crop->x,
            'y' => $default_saved_crop->y,
            'x1' => $default_saved_crop->x1,
            'y1' => $default_saved_crop->y1,
            'w' => $default_saved_crop->width,
            'h' => $default_saved_crop->height,
          );
        }
      }

      $element['styles']['set']['x'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
          'class' => array('jcrop-x', 'hidden'),
        ),
        '#value' => !empty($crops->x) ? $crops->x : $default_crops['x'],
      );

      $element['styles']['set']['y'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
          'class' => array('jcrop-y', 'hidden'),
        ),
        '#value' => !empty($crops->y) ? $crops->y : $default_crops['y'],
      );


      $element['styles']['set']['min-width'] = array(
        '#type' => 'hidden',
        '#value' => !empty($crops->width) ? $crops->width : 10,
        '#attributes' => array(
          'class' => array('jcrop-min-width', 'hidden'),
        ),
      );

      $element['styles']['set']['x1'] = array(
        '#type' => 'hidden',
        '#title' => 'X1',
        '#attributes' => array(
          'class' => array('jcrop-x1', 'hidden'),
        ),
        '#value' => !empty($crops->x1) ? $crops->x1 : $default_crops['x1'],
      );


      $element['styles']['set']['y1'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
          'class' => array('jcrop-y1', 'hidden'),
        ),
        '#default_value' => !empty($crops->y1) ? $crops->y1 : $default_crops['y1'],
      );

      $element['styles']['set']['height'] = array(
        '#type' => 'hidden',
        '#attributes' => array(
          'class' => array('jcrop-height', 'hidden'),
        ),
        '#default_value' => !empty($crops->height) ? $crops->height : $default_crops['h'],
      );

      foreach (array_keys(array_filter($field_instance_settings['xmedia_image_styles_to_crop'])) as $image_style) {
        $element['styles']['set']['styles'][$image_style]['id'] = array(
          '#type' => 'hidden',
          '#value' => !empty($saved_crops[$image_style]) ? $saved_crops[$image_style]->id : '',
        );

        $element['styles']['set']['styles'][$image_style]['changed'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'class' => array('hidden', 'changed', $image_style),
          ),
          '#value' => 0,
        );

        $element['styles']['set']['styles'][$image_style]['fid'] = array(
          '#type' => 'hidden',
          '#value' => !empty($saved_crops[$image_style]) ? $saved_crops[$image_style]->fid : '',
        );

        $element['styles']['set']['styles'][$image_style]['x'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'class' => array('hidden', 'x', $image_style),
          ),
          '#value' => !empty($saved_crops[$image_style]) ? $saved_crops[$image_style]->x : '',
        );

        $element['styles']['set']['styles'][$image_style]['y'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'class' => array('hidden', 'y', $image_style),
          ),
          '#default_value' => !empty($saved_crops[$image_style]) ? $saved_crops[$image_style]->y : '',
        );

        $element['styles']['set']['styles'][$image_style]['x1'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'class' => array('hidden', 'x1', $image_style),
          ),
          '#default_value' => !empty($saved_crops[$image_style]) ? $saved_crops[$image_style]->x1 : '',
        );

        $element['styles']['set']['styles'][$image_style]['y1'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'class' => array('hidden', 'y1', $image_style),
          ),
          '#default_value' => !empty($saved_crops[$image_style]) ? $saved_crops[$image_style]->y1 : '',
        );

        $element['styles']['set']['styles'][$image_style]['height'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'class' => array('hidden', 'height', $image_style),
          ),
          '#default_value' => !empty($saved_crops[$image_style]) ? $saved_crops[$image_style]->height : '',
        );

        $element['styles']['set']['styles'][$image_style]['width'] = array(
          '#type' => 'hidden',
          '#attributes' => array(
            'class' => array('hidden', 'width', $image_style),
          ),
          '#default_value' => !empty($saved_crops[$image_style]) ? $saved_crops[$image_style]->width : '',
        );
      }

      $element['styles']['set']['image'] = array(
        '#type' => 'markup',
        '#prefix' => "<div id=" . $crop_wrapper_id . ">",
        '#suffix' => "</div>",
        '#markup' => isset($image_properties['style_name']) ? theme_image_style($image_properties) : theme_image($image_properties),
      );

      $element['styles']['set']['width'] = array(
        '#type' => 'textfield',
        '#size' => 15,
        '#maxlength' => 15,
        '#prefix' => '<div class=jcrop-attribute>',
        '#suffix' => '</div>',
        '#attributes' => array(
          'class' => array('jcrop-width', 'hidden'),
        ),
        '#default_value' => !empty($crops->width) ? $crops->width : $default_crops['w'],
      );

      $element['styles']['set']['min-height'] = array(
        '#type' => 'hidden',
        '#prefix' => '<div class=jcrop-attribute>',
        '#suffix' => '</div>',
        '#value' => !empty($crops->height) ? $crops->height : 10,
        '#attributes' => array(
          'class' => array('jcrop-min-height', 'hidden'),
        ),
      );

      $element['styles']['set']['min-width'] = array(
        '#type' => 'hidden',
        '#prefix' => '<div class=jcrop-attribute>',
        '#suffix' => '</div>',
        '#value' => !empty($crops->width) ? $crops->width : 10,
        '#attributes' => array(
          'class' => array('jcrop-min-width', 'hidden'),
        ),
      );

      $element['styles']['set']['image'] = array(
        '#type' => 'markup',
        '#prefix' => "<div id=" . $crop_wrapper_id . ">",
        '#suffix' => "</div>",
        '#markup' => isset($image_properties['style_name']) ?
            theme_image_style($image_properties) :
            theme_image($image_properties),
      );
    }
    else {
      // Set a preview style when cropping is not enabled
      $image_properties['style_name'] = 'xmedia_library';
      $element['styles']['set']['image'] = array(
        '#type' => 'markup',
        '#prefix' => '<div id="image-preview">',
        '#suffix' => '</div>',
        '#markup' => theme_image_style($image_properties),
      );
    }
  }

  $element['actions'] = array(
    '#weight' => 100,
  );

  return $element;
}

/**
 * Callback for removing item
 * @todo: not used now - remove
 *
 * @param $form
 * @param $form_state
 * @return string
 */
function xmedia_ui_remove_one_submit($form, &$form_state) {
  // Element that triggered this action
  $button = $form_state['triggering_element'];
  // Go one level up in the form, to the widgets container.
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));

  $field_name = $element['#field_name'];
  $language = $element['#language'];
  $parents = $element['#field_parents'];
  $delta = $element['#delta'];

  // Get field state
  $field_state = field_form_get_state($parents, $field_name, $language, $form_state);
  $form_state['values'][$field_name][$language][$delta]['mid'] = NULL;
  $form_state['values'][$field_name][$language][$delta]['removed'] = TRUE;

  // Set field state with updated items_count
  field_form_set_state($parents, $field_name, $language, $form_state, $field_state);
  $form_state['rebuild'] = TRUE;
}

/**
 * Js callback for removing item
 * @todo: not used now - remove
 *
 * @param $form
 * @param $form_state
 * @return string
 */
function xmedia_ui_remove_one_js(&$form, &$form_state) {

  // Element that triggered this action
  $button = $form_state['triggering_element'];
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));

  $field_name = $element['#field_name'];
  $langcode = $element['#language'];

  $parents = $element['#field_parents'];
  $delta = $element['#delta'];

  $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
  $form_state['values'][$field_name][$langcode][$delta]['mid'] = NULL;
  field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);

  $field = $field_state['field'];

  if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
    return;
  }

  $form['rebuild'] = TRUE;
  return '';
}

function xmedia_ui_crop($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  $triggering_element = $form_state['triggering_element'];
  $aspect_ratio = 0;

  $element_name = end($triggering_element['#array_parents']);
  $origin_image_style = NULL;

  //@todo refactor this
  switch ($element_name) {
    case 'origin_image_style':
      $origin_image_style = $triggering_element['#value'];
      $nested_values = drupal_array_get_nested_value($form, array_slice($triggering_element['#array_parents'], 0, -1));
      $aspect_ratio = $nested_values['ratio']['#value'];
      $style_name = $nested_values['image_style']['#value'];
      break;

    case 'image_style':
      $style_name = $triggering_element['#value'];
      $nested_values = drupal_array_get_nested_value($form, array_slice($triggering_element['#array_parents'], 0, -1));
      $aspect_ratio = $nested_values['ratio']['#value'];
      $origin_image_style = $nested_values['origin_image_style']['#value'];
      break;

    case 'ratio':
      $nested_values = drupal_array_get_nested_value($form, array_slice($triggering_element['#array_parents'], 0, -1));
      $aspect_ratio = $triggering_element['#value'];
      $origin_image_style = $nested_values['origin_image_style']['#value'];
      $style_name = $nested_values['image_style']['#value'];
      break;
  }
  
  // Unfortunately we have no way to discover on which field/language/item was trigger submitted
  // We are parsing wrapper_id to get the required values
  // This is sad day for developers all across the world.
  $wrapper_id = $triggering_element['#ajax']['wrapper'];
  $data = explode('-', $wrapper_id);

  $field_name = $data[0];
  $language = $data[1];
  $delta = $data[2];

  if (empty($style_name)) {
    // no style selected, we are croping on original image
    $file = file_load($form_state['values'][$field_name][$language][$delta]['fid']);
    $image = image_load($file->uri);

    $init_coords = array(
      'x' => 0,
      'y' => 0,
      'x1' => $image->info['width'],
      'y1' => $image->info['height'],
      'w' => $image->info['width'],
      'h' => $image->info['height'],
    );
  }
  else {
    // style selected
    $saved_init_coords = $form_state['values'][$field_name][$language][$delta]['styles']['set']['styles'][$style_name];
    $style = image_style_load($style_name);
    $init_coords = xmedia_crop_get_default_coords_from_style($style);
    if (is_numeric($saved_init_coords['x'])) {
      $init_coords = array(
        'x' => $saved_init_coords['x'],
        'y' => $saved_init_coords['y'],
        'x1' => $saved_init_coords['x1'],
        'y1' => $saved_init_coords['y1'],
        'w' => $saved_init_coords['width'],
        'h' => $saved_init_coords['height'],
      );
    }
  }
  
  drupal_add_js(array(
    'xmediaCrop' => array(
      'ratio' => $aspect_ratio,
      'coords' => $init_coords,
      'wrapper_id' => $wrapper_id,
      'origin_image_style' => $origin_image_style,
    ),
  ), 'setting');

  $xmedia = xmedia_load($form_state['values'][$field_name][$language][$delta]['mid']);

  return xmedia_crop_form_style($xmedia, $origin_image_style);
}
