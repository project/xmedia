<?php

/**
 * Implements HOOK_field_info()
 * @return array
 */
function xmedia_ui_field_info() {
  return array(
    'xmedia' => array(
      'label' => t('XMedia'),
      'description' => t('This field stores a reference to a xmedia files.'),
      'settings' => array(
        'xmedia_display_bundle',
        'xmedia_image_styles_to_crop',
        'xmedia_default_crop_style' => 'large',
        'xmedia_lock_aspect_ratio' => 0,
      ),
      'default_widget' => 'xmedia_default_widget',
      'default_formatter' => 'xmedia_default_formatter',
    ),
  );
}

/**
 * Implements HOOK_field_settings_form()
 * @param $field
 * @param $instance
 * @param $has_data
 * @return array
 */
function xmedia_ui_field_settings_form($field, $instance, $has_data) {
  $settings = $field['settings'];
  $form = array();

  switch ($field['type']) {
    case 'xmedia':

      $xmedia_display_bundles = xmedia_display_get_bundles();
      $xmedia_display_bundle_options = array();
      foreach($xmedia_display_bundles as $bundle_name => $bundle_info) {
        $xmedia_display_bundle_options[$bundle_name] = $bundle_info->title;
      }

      $form['xmedia_display_bundle'] = array(
        '#type' => 'select',
        '#title' => t('XMedia Display bundle'),
        '#description' => t('Select Xmedia Display entity bundle that will be used to store informations for this field.'),
        '#default_value' => (!empty($settings['xmedia_display_bundle']) ? $settings['xmedia_display_bundle'] : reset(array_keys($xmedia_display_bundle_options))),
        '#options' => $xmedia_display_bundle_options,
        '#required' => TRUE,
      );

      $form['xmedia_default_crop_style'] = array(
        '#type' => 'select',
        '#description' => t('Select default image style for "Crop from image style"'),
        '#title' => t('Crop from image style'),
        '#default_value' => (!empty($settings['xmedia_default_crop_style']) ? $settings['xmedia_default_crop_style'] : reset(array_keys(xmedia_db_get_bundle_options()))),
        '#options' => xmedia_get_image_styles_options(),
        '#required' => TRUE,
      );

      $form['xmedia_lock_aspect_ratio'] = array(
        '#type' => 'checkbox',
        '#title' => t('Lock aspect ratio'),
        '#default_value' => $settings['xmedia_lock_aspect_ratio'],
      );

      $form['xmedia_image_styles_to_crop'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Select image styles.'),
        '#default_value' => (!empty($settings['xmedia_image_styles_to_crop']) ? $settings['xmedia_image_styles_to_crop'] : array()),
        '#options' => xmedia_get_image_styles_options(),
        '#required' => TRUE,
      );
      break;
  }

  return $form;
}

/**
 * Implements HOOK_field_is_empty().
 * @param $item
 * @param $field
 * @return bool
 */
function xmedia_ui_field_is_empty($item, $field) {

  if (isset($item['_remove']) && $item['_remove'] == 1 && isset($item['mid'])) {
    $item['mid'] = 0;
  }

  if (empty($item['mid'])) {
    if (isset($item['display_mid']) && $item['display_mid'] != 0 && isset($item['mid'])) {
      // deleting functionality
      $xmedia_display = xmedia_display_load($item['display_mid']);

      if ($xmedia_display) {
        xmedia_display_delete($xmedia_display);
      }
    }
    return TRUE;
  }

  if(is_string($item)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements HOOK_field_widget_info().
 * @return array
 */
function xmedia_ui_field_widget_info() {
  return array(
    'xmedia_default_widget' => array(
      'label' => t('XMedia selector'),
      'field types' => array('xmedia'),
      'behaviors' => array(
        // We need to override default behaviour for adding new items - FIELD_BEHAVIOUR_CUSTOM
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
  );
}

/**
 * Implements HOOK_field_widget_form()
 * @param $form
 * @param $form_state
 * @param $field
 * @param $instance
 * @param $langcode
 * @param $items
 * @param $delta
 * @param $element
 * @return array
 */
function xmedia_ui_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  switch ($instance['widget']['type']) {
    case "xmedia_default_widget":

      // empty settings for cropping -- needed for default initialization ? not sure why
      drupal_add_js(array('xmediaCrop' => array('key' => 'null')), 'setting');
      // attach javascript for fancyBox browser and styles
      $form['#attached'] = array(
        'js' => array(
          drupal_get_path('module', 'xmedia_ui') . '/js/jquery.fancybox.js', // fancyBox library
          drupal_get_path('module', 'xmedia_ui') . '/js/xmedia_ui.js', // media browser
          drupal_get_path('module', 'xmedia_crop') . '/js/jquery.jcrop.js', // crop library
          drupal_get_path('module', 'xmedia_crop') . '/js/xmedia_crop.js'
        ), // crop implementation
        'css' => array(
          drupal_get_path('module', 'xmedia_ui') . '/css/jquery.fancybox.css', // fancyBox css
          drupal_get_path('module', 'xmedia_crop') . '/css/jquery.jcrop.css', // crop css
          drupal_get_path('module', 'xmedia_ui') . '/css/xmedia_ui.css', // browser css
        ),
      );

      $field_name = $field['field_name'];
      $parents = $form['#parents'];

      // Get DELTA count
      $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
      $max = $field_state['items_count'];

      $title = check_plain($instance['label']);
      $description = field_filter_xss($instance['description']);

      $id_prefix = implode('-', array_merge($parents, array($field_name)));
      $wrapper_id = drupal_html_id($id_prefix . '-add-more-wrapper');

      $field_elements = array();

      // cycling deltas
      for ($delta = 0; $delta <= $max; $delta++) {
        if ($max > 0 && $delta == $max) {
          continue;
        }
        $multiple = $field['cardinality'] > 1 || $field['cardinality'] == FIELD_CARDINALITY_UNLIMITED;

        $element_wrapper_id = drupal_html_id($id_prefix . '-element-' . $delta);

        $items[$delta]['element_wrapper_id'] = $element_wrapper_id;

        $element = array(
          '#entity_type' => $instance['entity_type'],
          '#bundle' => $instance['bundle'],
          '#field_name' => $field_name,
          '#language' => $langcode,
          '#field_parents' => $parents,
          '#columns' => array_keys($field['columns']),
          // For multiple fields, title and description are handled by the wrapping table.
          '#title' => $multiple ? '' : $title,
          '#description' => $multiple ? '' : $description,
          // Only the first widget should be required.
          '#delta' => $delta,
          '#weight' => $delta,
          '#prefix' => "<div id=$element_wrapper_id>",
          '#suffix' => "</div>",
          // XMEDIA!
          '#type' => 'xmedia',
          '#item' => isset($items[$delta]) ? $items[$delta] : array(),
          '#theme' => 'xmedia_ui_element',
        );

        // adding _remove, this is used to mark media files for removal
        // it is not possible(not in a nice way - using form ajax) to remove medias on the run.
        // we are just marking medias for removal - actual removal is done in hook_entity_presave
        // position if this element is done in xmedia_multiple_value_form theme - neat!
        $element['_remove'] = array(
          '#type' => 'checkbox',
          '#attributes' => array('class' => array('xmedia-remove-item-checkbox')),
        );

        // adding _weight, this is used for sorting in 'xmedia_multiple_value_form'
        $element['_weight'] = array(
          '#type' => 'weight',
          '#title' => t('Weight for row @number', array('@number' => $delta + 1)),
          '#title_display' => 'invisible',
          '#delta' => $max,
          '#attributes' => array('style' => 'display:none'),
          '#default_value' => isset($items[$delta]['_weight']) ? $items[$delta]['_weight'] : $delta,
        );

        $field_elements[$delta] = $element;
      }

      $field_elements += array(
        '#theme' => 'xmedia_multiple_value_form',
        '#field_name' => $field['field_name'],
        '#cardinality' => $field['cardinality'],
        '#title' => $title,
        '#required' => $instance['required'],
        '#element_validate' => array('xmedia_ui_element_validate'),
        '#description' => $description,
        '#prefix' => '<div id="' . $wrapper_id . '">',
        '#suffix' => '</div>',
        '#max_delta' => $max,
      );

      $add_more_id = strtr($id_prefix, '-', '_') . '_add_more';
      // add more button
      $field_elements['add_more'] = array(
        '#type' => 'submit',
        '#name' => $add_more_id,
        '#id' => $add_more_id,
        '#value' => t('Add another item'),
        '#attributes' => array('class' => array('field-add-more-submit')),
        '#limit_validation_errors' => array(array_merge($parents, array($field_name, $langcode))),
        '#submit' => array('xmedia_ui_add_more_submit'),
        '#attributes' => array('class' => array('xmedia-add-more-button')),
        '#ajax' => array(
          'callback' => 'xmedia_ui_add_more_js',
          'wrapper' => $wrapper_id,
          'effect' => 'fade',
        ),
      );

      drupal_add_js(array(
        'xmedia' => array(
          'add_more_id' => $add_more_id,
        ),
      ), 'setting');

      // link (looks like button) for fancyBox browser
      $field_id = strtr($id_prefix, '-', '_');
      $field_elements['browse'] = array(
        '#type' => 'markup',
        '#markup' => l('Browse', 'xmedia-browser/' . $field_id, array(
          'attributes' => array(
            'class' => array('xmedia-browser-modal', 'button')
          )
        )),
      );

      // add field element for storing data string of XMedia ids we want to add (1,2,3,4, ...)
      $mid_elements_id = strtr($id_prefix, '-', '_') . '_mid_elements';
      $field_elements['mid_elements'] = array(
        '#type' => 'textfield',
        '#id' => $mid_elements_id,
        '#attributes' => array('class' => array('hidden'), 'style' => "display:none"),
      );

      return $field_elements;
      break;
  }
}

/**
 * Submit callback, this is used even if ajax callback is defined.
 * Creating as many elements as needed, mostly important we are setting items_count
 *
 * @param $form
 * @param $form_state
 */
function xmedia_ui_add_more_submit($form, &$form_state) {

  // Element that triggered this action
  $button = $form_state['triggering_element'];
  // Go one level up in the form, to the widgets container.
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));

  $field_name = $element['#field_name'];
  $language = $element['#language'];
  $parents = $element['#field_parents'];

  // Get field state
  $field_state = field_form_get_state($parents, $field_name, $language, $form_state);
  if (!isset($field_state['items_count'])) {
    $field_state['items_count'] = 0;
  }

  $id_string = $form_state['values'][$field_name][$language]['mid_elements'];
  $media_ids = explode(',', trim($id_string, ','));

  foreach ($media_ids as $delta => $id) {
    // Set XMedia #ID to as many elements as needed
    // All required fields will be rendered afterwards
    $next_item = $field_state['items_count'] + $delta;
    $form_state['values'][$field_name][$language][$next_item]['mid'] = $id;
  }

  // Raise the items count
  $field_state['items_count'] += count($media_ids);

  // Set field state with updated items_count
  field_form_set_state($parents, $field_name, $language, $form_state, $field_state);
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback for replacing element
 * @param $form
 * @param $form_state
 * @return array|null
 */
function xmedia_ui_add_more_js($form, $form_state) {

  // Element that triggered this action
  $button = $form_state['triggering_element'];
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));

  $field_name = $element['#field_name'];
  $langcode = $element['#language'];

  $parents = $element['#field_parents'];

  $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
  $field = $field_state['field'];

  return $element;
}

function xmedia_ui_element_validate($element, &$form_state, $form) {
  $button = $form_state['triggering_element'];
  $field_name = $element['#field_name'];
  $language = $element['#language'];
  $parents = $element['#field_parents'];

  // Get field state
  $items_count = 0;
  foreach($form_state['values'][$field_name]['und'] as $delta => $item) {
    if(is_numeric($delta) && is_array($item) && isset($item['mid']) && $item['_remove'] != TRUE) {
      $items_count++;
    }
  }
  if ($element['#required'] == TRUE && $button['#name'] != $element['add_more']['#name'] && $items_count == 0) {
    form_error($element, t('Field %field is required. Please select some image from Xmedia library.', array('%field' => $element['#title'])));
  }
}

/**
 * Implementation of HOOK_entity presave
 *
 * We need to check entity for xmedia field.
 * If present, we are creating new entity of xmedia display.
 * According to field settings we need to set xmedia display bundle.
 *
 * Copying XMedia entity into new XMedia display entity.
 * This provides us with total independence between backend of xmedia (XMedia entity)
 * and fronted of xmedia ( XMedia display )
 *
 * Note: we are using this hook, because there is no way to add submit process callback for our element
 * ( not working at least )
 *
 * @param $entity
 *  Entity name
 * @param $type
 *  Entity bundle
 * @return void
 */
function xmedia_ui_entity_presave($entity, $type) {

  // We dont want to do this for our entities, because recursion issue may occur
  if ($type == 'xmedia' || $type == 'xmedia_display') {
    return;
  }

  $crop_path = 'public://' . variable_get('xmedia_ui_filepath', 'crops') . '/';

  // Foreach existing xmedia field
  foreach ($entity as $field_name => $field) {
    $field_info = field_info_field($field_name);

    //check if field is of type 'xmedia'
    if(!is_null($field_info) && $field_info['module'] == 'xmedia_ui' && $field_info['type'] == 'xmedia') {

      // Field is present, now we know we are creating new xmedia_display entity
      $items = $field;

      // Loop trough each items in our field instance
      // @todo language support
      $controller = entity_get_controller('xmedia_display');

      $fitems['und'] = array();
      foreach ($items['und'] as $delta => &$item) {
        if (is_numeric($delta) && is_array($item)) {
          $fitems['und'][] = $item;
        }
      }

      // we are done with the xmedia_field if there are no items
      if (!count($fitems['und'])) {
        $entity->{$field_name} = $fitems;
        continue;
      }

      foreach ($fitems['und'] as &$item) {
        $xmedia_display = $item['entity']['xmedia_display'];

        // Loop trough all fields which where attached to element and add them to our entity,
        // assigning all fields to our entity
        $xmedia_display->title = $item['image_info']['title'];
        $xmedia_display->alternate_text = $item['image_info']['alternate_text'];
        $xmedia_display->caption = $item['image_info']['caption'];
        $origin_image_style = $item['image_info']['origin_image_style'];

        // load original file
        $file = file_load($item['fid']);
        // duplicate our image so we can perform crop later
        $realpath = drupal_realpath($file->uri);

        $file_item = array(
          'fid'     => $file->fid,
          'alt'     => $xmedia_display->alternate_text,
          'title'   => $xmedia_display->title,
        );

        $xmedia_display->xmedia_image['und'][0] = $file_item;

        // Get all instances for our display bundle
        $bundle_instances = field_info_instances('xmedia_display', $field_info['settings']['xmedia_display_bundle']);
        foreach ($bundle_instances as $delta => $bundle_instance) {
          if($delta == 'xmedia_image') {
            continue;
          }
          // field might be empty and thus filtered out by field api
          $field_value = $item['add_fields'][$delta];
          if (isset($field_value)) {
            unset($field_value['und']['add_more']);

            $field_value['und'] = _field_filter_items(field_info_field($delta), $field_value['und']);
            $xmedia_display->$delta = $field_value;
          }
        }

        $controller->save($xmedia_display);

        foreach ($item['styles']['set']['styles'] as $image_style => $style_to_crop) {
          if ($style_to_crop['changed']) {

            if ($style_to_crop['fid']) {
              $old_file = file_load($style_to_crop['fid']);
              if($old_file) {
                file_delete($old_file);
              }
            }

            $new_file = file_save_data(file_get_contents($realpath), $crop_path . $file->filename, FILE_EXISTS_RENAME);

            $crop = new XMediaCrop();
            $crop->populateFromArray($style_to_crop);
            $crop->origin_image_style = $origin_image_style;
            $crop->image_style        = $image_style;
            $crop->entity_id          = $xmedia_display->id;
            $crop->ratio              = $item['image_info']['ratio'];
            $crop->fid                = $new_file->fid;

            xmedia_crop_db_save($crop);

            $image = image_load($new_file->uri);
            $dimensions_origin = xmedia_ui_get_image_style_dimensions($crop->origin_image_style, $crop);
            $dimensions_target = xmedia_ui_get_image_style_dimensions($crop->image_style, $crop);
            $scale = $image->info['width'] / $dimensions_origin['width'];
            $scale_data = array('width' => round($dimensions_origin['width'] * $scale), 'height' => round($dimensions_origin['height'] * $scale), 'upscale' => $dimensions_origin['upscale']);

            if($scale > 1) {
              image_crop($image, round($crop->x * $scale), round($crop->y * $scale), round($crop->width * $scale), round($crop->height * $scale));
              image_scale_effect($image, array('width' => $dimensions_target['width'], 'height' => $dimensions_target['height'], 'upscale' => TRUE));
            }
            else if ($dimensions_origin['upscale'] == FALSE) {
              image_crop($image, $crop->x, $crop->y, $crop->width, $crop->height);
            }
            else {
              // TODO: Upscale doesn't work!!
              image_scale_effect($image, $scale_data);
              image_crop($image, round($crop->x * $scale), round($crop->y * $scale), round($crop->width * $scale), round($crop->height * $scale));
            }
            image_save($image);

            file_save($new_file);

            image_path_flush($new_file->uri);
          }
        }

        // We need to update original xmedia field with our newly created entity_id
        $entity_info = entity_get_info($type);
        $entity_keys = $entity_info['entity keys'];
        $item['display_mid'] = $xmedia_display->id;
      }

      // we did a little alter on field... lets make it count
      $entity->{$field_name} = $fitems;
    }
  }
}

function xmedia_ui_db_save_coords($data) {
  $result = FALSE;

  $data['entity_id'] = 0;
  $data['created'] = time();
  $data['modified'] = time();

  if (!empty($data['id'])) {
    $data['modified'] = time();
    $result = drupal_write_record('xmedia_crops', $data, array('id'));
  }
  else {
    $data['created'] = time();
    $result = drupal_write_record('xmedia_crops', $data);
  }

  return $data['id'];
}

/**
 * Implements HOOK_field_formatter_info().
 * @return array
 */
function xmedia_ui_field_formatter_info() {

  $formatters = array(
    'xmedia_default_formatter' => array(
      'label' => t('Entity display'),
      'field types' => array('xmedia'),
      'settings' => array(
        'xmedia_display' => 'default',
      ),
    ),
    'xmedia_image' => array(
      'label' => t('Xmedia formatter'),
      'field types' => array('image'),
      'settings' => array(
        'xmedia_image_style' => 'thumbnail',
      ),
    ),
  );

  return $formatters;
}

/**
 * Implementation of field_formatter_settings_form
 * @param $field
 * @param $instance
 * @param $view_mode
 * @param $form
 * @param $form_state
 * @return array
 */
function xmedia_ui_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

  $display = $instance['display'][$view_mode];

  $settings = $display['settings'];
  $element = array();

  switch ($display['type']) {

    case 'xmedia_default_formatter':
      $entity_info = entity_get_info('xmedia_display');

      foreach ($entity_info['view modes'] as $delta => $view_mode) {
        $options[$delta] = $view_mode['label'];
      }

      $element['xmedia_display'] = array(
        '#type' => 'select',
        '#title' => t('View mode'),
        '#description' => t('Select view mode of Xmedia Display that you want to display.'),
        '#default_value' => $settings['xmedia_display'],
        '#required' => TRUE,
        '#options' => $options,
      );
      break;

    case 'xmedia_image':
      $image_styles = image_styles();
      $options = array();
      foreach ($image_styles as $image_style => $style_options) {
        $options[$image_style] = $style_options['name'];
      }
      $element['xmedia_image_style'] = array(
        '#title' => t('Image style'),
        '#type' => 'select',
        '#default_value' => $settings['xmedia_image_style'],
        '#options' => $options,
      );
      break;
  }

  return $element;
}

/**
 * Implementation of field_formatter_settings_summary
 * @param $field
 * @param $instance
 * @param $view_mode
 * @return string
 */
function xmedia_ui_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];

  switch ($display['type']) {
    case 'xmedia_default_formatter':
      $xmedia_display_delta = $display['settings']['xmedia_display'];
      $entity_info = entity_get_info('xmedia_display');
      $xmedia_display = $entity_info['view modes'][$xmedia_display_delta]['label'];
      return t('View mode: @display', array('@display' => $xmedia_display));
      break;
    case 'xmedia_image':
      $image_styles = image_styles();
      $selected_style = $image_styles[$display['settings']['xmedia_image_style']]['name'];
      return t('Image style: @image_style', array('@image_style' => $selected_style));
      break;
  }
}

function xmedia_ui_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, &$items, $display) {
  $formatter = $display['type'];
  $element = array();

  switch ($formatter) {
    case 'xmedia_default_formatter':
      // default formatter for displaying whole entity as attached field
      foreach ($items as $delta => $item) {

        $xmedia_display = entity_load('xmedia_display', array($item['display_mid']));
        $element[$delta]['#markup'] = xmedia_display_view(array_shift($xmedia_display), $display['settings']['xmedia_display']);
      }

      break;

    case 'xmedia_image':
      // formatter for displaying image
      foreach ($items as $delta => $item) {
        if ($display['settings']['xmedia_image_style']) {

          $crop   = xmedia_crop_db_get_by_entity_id_and_style($entity->id, $display['settings']['xmedia_image_style']);
          if($crop != null) {
            $image  = file_load($crop->fid);
            $variables = array(
              'path'    => $image->uri,
              'attributes' => array(),
            ) + $item;
            unset($variables['width'], $variables['height']);
            $element[$delta]['#markup'] = theme_image($variables);
          }
        }
        else {
          $element[$delta]['#markup'] = theme_image(array(
            'path' => $item['uri'],
            'attributes' => array(),
              ) + $item);
        }
      }
      break;
    default;
      break;
  }

  return $element;
}

/**
 * View xmedia_display entity
 *
 * @param $xmedia_display
 * @param string $view_mode
 * @param string $langcode
 * @return bool|string
 */
function xmedia_display_view($xmedia_display, $view_mode = "display", $langcode = NULL) {

  $xmedia_display->content = array();

  if (!isset($langcode)) {
    $langcode = $GLOBALS['language_content']->language;
  }
  field_attach_prepare_view('xmedia_display', array($xmedia_display->id => $xmedia_display), $view_mode, $langcode);
  entity_prepare_view('xmedia_display', array($xmedia_display->id => $xmedia_display));

  $xmedia_display->content += field_attach_view('xmedia_display', $xmedia_display, $view_mode);

  $xmedia_display->content['title'] = array(
    '#markup' => '<div class="xmedia-title">' . $xmedia_display->title . '</div>',
  );

  $xmedia_display->content['alternate_text'] = array(
    '#markup' => '<div class="xmedia-alt-text">' . $xmedia_display->alternate_text . '</div>',
  );

  $build = $xmedia_display->content;
  unset($xmedia_display->content);

  $build += array(
    '#theme' => 'xmedia_display',
    '#xmedia_display' => $xmedia_display,
    '#view_mode' => $view_mode,
    '#language' => $langcode,
  );

  return drupal_render($build);
}

function template_preprocess_xmedia_display(&$variables) {
  $variables['view_mode'] = $variables['elements']['#view_mode'];

  // Provide a distinct $teaser boolean.
  $variables['xmedia_display'] = $variables['elements']['#xmedia_display'];
  $xmedia_display = $variables['xmedia_display'];

  // Flatten the xmedia object's member fields.
  $variables = array_merge((array) $xmedia_display, $variables);

  // Helpful $content variable for templates.
  $variables += array('content' => array());
  foreach (element_children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
  // Make the field variables available with the appropriate language.
  field_attach_preprocess('xmedia_display', $xmedia_display, $variables['content'], $variables);
}

/**
 * Helper function to get basic width and height setting of an image style.
 * @param type $style_name
 *  Image style name.
 * @param type $crop
 *  Optional crop settings.
 * @return array
 *  Return an array with 'width' and 'height' keys.
 */
function xmedia_ui_get_image_style_dimensions($style_name, $crop = null) {
  $style = image_style_load($style_name);
  $upscale = FALSE;
  if(is_null($crop)) {
    $width = 0;
    $height = 0;
  } else {
    $width = $crop->width;
    $height = $crop->width;
  }

  if($style) {
    foreach($style['effects'] as $effect) {
      if($effect['module'] == 'image' && $effect['name'] == 'image_scale') {
        $width = $effect['data']['width'];
        $height = $effect['data']['height'];
        $upscale = $effect['data']['upscale'];
        break;
      }
    }
  }

  return array('width' => $width, 'height' => $height, 'upscale' => $upscale);
}
