<?php

function xmedia_ui_library_form($form, &$form_state, $medias = array()) {
  $form = array();

  $form['#id'] = 'xmedia-library-browser';

  $form['ids'] = array(
    '#type' => 'value',
    '#value' => array_keys($medias),
  );

  if (is_array($medias)) {
    foreach ($medias as $key => $media) {
      $xmedia = new XMedia();
      $xmedia->populateFromObj($media);

      $form['items'] = array(
        '#type' => 'container',
      );

      $form['items'][$media->id] = array(
        '#type' => 'container',
        '#id' => $media->id,
        '#attributes' => array(
          'class' => array(
            'xmedia-selectable', 'xmedia-not-selected',
          ),
        ),
      );

      $form['items'][$media->id]['xmedia'] = array(
        '#type' => 'value',
        '#value' => $xmedia,
      );

    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Add images',
  );

  return $form;
}

function xmedia_ui_library_form_validate($form, &$form_state) {

}

function xmedia_ui_library_form_submit($form, &$form_state) {

}


function xmedia_display_type_form($form, &$form_state, $bundle = NULL, $op = NULL) {
  $form = array();

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => isset($bundle->id) ? $bundle->id : '',
  );

  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('The human-readable name of this content type. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#default_value' => isset($bundle->title) ? $bundle->title : '',
  );

  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('The machine-readable name of this content type. It is recommended that this code contain only letters, numbers, and underscores. This name must be unique.'),
    '#default_value' => isset($bundle->type) ? $bundle->type : '',
  );
  if (empty($bundle->id)) {
    $form['source_type'] = array(
      '#title' => t('Source type'),
      '#type' => 'select',
      '#required' => TRUE,
      '#description' => t('Caution! Once set, this value cannot be changed.'),
      '#options' => xmedia_db_get_bundle_options(),
      '#default_value' => !empty($bundle->source_type) ? xmedia_db_get_bundle_options($bundle->source_type) : '',
    );
  }

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#description' => t('Describe this media type. The text will be displayed on the Add new media.'),
    '#default_value' => isset($bundle->description) ? $bundle->description : '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save media type'),
    '#submit' => array(
      'xmedia_ui_type_form_submit',
    ),
  );

  return $form;
}

function xmedia_ui_type_form_submit($form, &$form_state) {
  $model_type = entity_ui_form_submit_build_entity($form, $form_state);
  $model_type->save();

  $form_state['redirect'] = 'admin/structure/xmedia-ui';
}

/**
 * Xmedia UI settings
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function xmedia_ui_settings_form($form, &$form_state) {

  $form['xmedia_ui_filepath'] = array(
    '#title' => t('File path'),
    '#description' => t('Temporary path for storing temporary cropped images.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('xmedia_ui_filepath', 'crops'),
    '#field_prefix' => 'public://',
    '#required' => TRUE,
    '#after_build' => array('xmedia_ui_check_directory'),
  );

  $form['xmedia_ui_browser_view'] = array(
    '#title' => t('Browser view'),
    '#description' => t('View to use for the xmedia browser (machine name).'),
    '#type' => 'textfield',
    '#default_value' => variable_get('xmedia_ui_browser_view', 'xmedia_browser'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}


function xmedia_ui_check_directory($form_element) {
  $directory = drupal_realpath('public://' . $form_element['#value']);
  if (strlen($directory) == 0) {
    return $form_element;
  }

  if (!is_dir($directory) && !drupal_mkdir($directory, NULL, TRUE)) {
    // If the directory does not exists and cannot be created.
    form_set_error($form_element['#parents'][0], t('The directory %directory does not exist and could not be created.', array('%directory' => $directory)));
    watchdog('file system', 'The directory %directory does not exist and could not be created.', array('%directory' => $directory), WATCHDOG_ERROR);
  }

  if (is_dir($directory) && !is_writable($directory) && !drupal_chmod($directory)) {
    // If the directory is not writable and cannot be made so.
    form_set_error($form_element['#parents'][0], t('The directory %directory exists but is not writable and could not be made writable.', array('%directory' => $directory)));
    watchdog('file system', 'The directory %directory exists but is not writable and could not be made writable.', array('%directory' => $directory), WATCHDOG_ERROR);
  }

  elseif (is_dir($directory)) {
    // Create public .htaccess file.
    drupal_set_message(t('Directory exists and is writeable'));
    file_create_htaccess($directory, FALSE);
  }

  return $form_element;
}

function xmedia_ui_insert_form($form, &$form_state, XMediaEntity $xmedia) {
  
  $form['#xmedia'] = $xmedia;
  
  $form['image_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image info'),
    '#collapsible' => FALSE,
  );
  
  $form['image_info']['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#default_value' => isset($xmedia->caption) ? $xmedia->caption : '',
    '#size' => 40,
    '#weight' => 1,
  );
  
  $form['image_info']['alt'] = array(
    '#title' => t('Alternate text'),
    '#type' => 'textfield',
    '#default_value' => isset($xmedia->alternate_text) ? $xmedia->alternate_text : '',
    '#size' => 40,
    '#weight' => 2,
  );
  
  $form['public_path'] = array(
    '#type' => 'hidden',
    '#value' => file_create_url('public://'),
  );
  
  $xmedia_image = field_get_items('xmedia', $xmedia, 'xmedia_image');
  $image_path = str_replace(array('public://', 'private://'), '', $xmedia_image[0]['uri']);
  
  $form['image_path'] = array(
    '#type' => 'hidden',
    '#value' => $image_path,
  );
  
  return $form;
}