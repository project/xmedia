<?php

require_once 'xmedia_ui.theme.inc';
require_once 'xmedia_ui.elements.inc';
require_once 'xmedia_ui.fields.inc';
require_once 'xmedia_ui.forms.inc';
require_once 'xmedia_ui.db.inc';
require_once 'xmedia_ui.controller.inc';
require_once 'xmedia_ui.entity.inc';

define('XMEDIA_UI_IMAGE_STYLE', 'xmedia_thumbnail');
define('XMEDIA_UI_BROWSER_WIZARD', 'wizard');

/**
 * Implements xmedia_ui_admin_paths
 */
function xmedia_ui_admin_paths() {
  return array(
    'xmedia-browser' => TRUE,
    'xmedia-browser/*' => TRUE,
    'xmedia-browser/*/*' => TRUE
  );
}

/**
 * Implements hook_menu().
 */
function xmedia_ui_menu() {
  $items = array();

  $items['admin/config/xmedia'] = array(
    'title' => 'xmedia',
    'description' => 'XMedia configuration',
    'position' => 'left',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer xmedia settings'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/xmedia/ui/settings'] = array(
    'title' => 'UI settings',
    'description' => 'Settings for the Xmedia UI',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('xmedia_ui_settings_form'),
    'access arguments' => array('administer xmedia settings'),
    'file' => 'xmedia_ui.forms.inc',
    'weight' => 1,
  );

  $items['xmedia-browser/%'] = array(
    'title' => 'XMedia browser',
    'page callback' => 'xmedia_ui_browser_page',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'file' => 'xmedia_ui.pages.inc',
    'type' => MENU_CALLBACK,
  );

  $items['xmedia-browser/%/browse'] = array(
    'title' => 'Browse',
    'page callback' => 'xmedia_ui_browser_page',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'file' => 'xmedia_ui.pages.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['xmedia-browser/%/upload'] = array(
    'title' => 'Upload',
    'page callback' => 'xmedia_ui_library_page',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'file' => 'xmedia_ui.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['xmedia-browser/wizard/insert/%'] = array(
    'title' => 'Insert image',
    'page callback' => 'xmedia_ui_insert_page',
    'page arguments' => array(3),
    'access callback' => TRUE,
    'file' => 'xmedia_ui.pages.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_entity_info()
 *
 * @return array
 */
function xmedia_ui_entity_info() {

  $entities['xmedia_display'] = array(
    'label' => t('XMedia display'),
    'controller class' => 'XMediaDisplayController',
    'views controller class' => 'XMediaDisplayViewsController',
    'metadata controller class' => 'XMediaDisplayMetaDataController',
    'entity class' => 'XMediaDisplayEntity',
    'view callback' => 'xmedia_display_view',
    'load hook' => 'xmedia_display_load',
    'base table' => 'xmedia_display',
    'fieldable' => TRUE,
    'module' => 'xmedia_ui',
    'entity keys' => array(
      'id' => 'id',
      'bundle' => 'type',
      'label' => 'title',
    ),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'bundles' => array(
      'display' => array(
        'label' => t('Display'),
      ),
    ),
    'view modes' => array(
      'default' => array(
        'label' => t('Default'),
        'custom settings' => FALSE,
      ),
    ),
    'static cache' => TRUE,
  );

  $entities['xmedia_display_type'] = array(
    'label' => t('Xmedia Display Type'),
    'entity class' => 'XMediaDisplayType',
    'controller class' => 'XMediaDisplayTypeController',
    'base table' => 'xmedia_display_type',
    'fieldable' => FALSE,
    'bundle of' => 'xmedia_display',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'title',
    ),
    'module' => 'xmedia_ui',
    'admin ui' => array(
      'path' => 'admin/structure/xmedia-ui',
      'file' => 'xmedia_ui.entity.inc',
      'controller class' => 'XMediaDisplayTypeUIController',
      'menu wildcard' => '%xmedia_display_type',
    ),
    'access callback' => 'xmedia_display_type_access',
  );

  return $entities;
}

/**
 * Access callback for xmedia display bundles
 * @param $op
 * @param $xmedia_display
 * @param null $account
 * @param null $entity_type
 * @return bool
 */
function xmedia_display_type_access($op, $xmedia_display, $account = NULL, $entity_type = NULL) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }

  switch ($op) {
    // Only one permission for managing bundles
    case 'create':
    case 'view':
    case 'edit':
      return user_access('administer xmedia types', $account);
  }

  return TRUE;
}

/**
 * Implements HOOK_entity_info_alter()
 * @param $entity_info
 */
function xmedia_ui_entity_info_alter(&$entity_info) {
  $bundles = xmedia_display_get_bundles();
  if (is_array($bundles)) {
    foreach ($bundles as $bundle) {
      $entity_info['xmedia_display']['bundles'][$bundle->type] = array(
        'label' => $bundle->title,
        'admin' => array(
          'path' => 'admin/structure/xmedia-ui/manage/%xmedia_display_type',
          'real path' => 'admin/structure/xmedia-ui/manage/' . $bundle->type,
          'bundle argument' => 4,
          'access arguments' => array('manage xmedia files'),
        ),
      );
    }
  }
}

/**
 * Implements HOOK_image_default_styles()
 * @return array
 */
function xmedia_ui_image_default_styles() {

  $styles = array();
  $styles['xmedia_library'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 270,
          'height' => 270,
          'upscale' => 1,
        ),
        'weight' => 0,
      ),
    )
  );

  return $styles;
}

function xmedia_ui_add_default_fields($bundle = 'default') {

  $field = field_info_field('xmedia_image');

  if (empty($field)) {
    $field = array(
      'field_name' => 'xmedia_image',
      'type' => 'image',
      'entity_types' => array('xmedia', 'xmedia_display'),
    );
    field_create_field($field);
  }
  
  if (is_null(field_info_instance('xmedia_display', 'xmedia_image', $bundle))) {
    $instance = array(
      'field_name' => 'xmedia_image',
      'entity_type' => 'xmedia_display',
      'bundle' => $bundle,
      'label' => t('Xmedia image'),
      'widget' => array(
        'weight' => 0,
        'type' => 'image_image'
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'xmedia_image',
          'module' => 'image',
          'weight' => 1,
        ),
        'display' => array(
          'label' => 'hidden',
          'type' => 'xmedia_image',
          'module' => 'image',
          'weight' => 1,
        ),
      ),
    );

    field_create_instance($instance);
  }
}

/**
 * Implementation of HOOK_permission()
 * @return array
 */
function xmedia_ui_permission() {
  return array(
    'administer media display types' => array(
      'title' => t('Administer xmedia display bundles'),
      'description' => t('Allows to access xmedia display bundles administration'),
    )
  );
}

/**
 * Implements hook_library().
 */
function xmedia_ui_library() {
  $libraries = array();

  $libraries['xmedia_ui'] = array(
    'title' => 'XMedia UI',
    'version' => '0.1',
    'css' => array(
      drupal_get_path('module', 'xmedia_ui') . '/css/xmedia_ui.css' => array(
        'type' => 'file',
        'media' => 'screen',
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements HOOK_field_extra_fields()
 */
function xmedia_ui_field_extra_fields() {
  $fields = array(
    'title' => array(
      'label' => t('Title'),
      'description' => t('XMedia entity meta'),
      'weight' => -5,
    ),
    'alternate_text' => array(
      'label' => t('Alt. text'),
      'description' => t('XMedia entity meta'),
      'weight' => -3,
    ),
    'caption' => array(
      'label' => t('Caption'),
      'description' => t('XMedia entity meta'),
      'weight' => -2,
    ),
    'meta_dimension' => array(
      'label' => t('Dimensions'),
      'description' => t('XMedia entity meta'),
      'weight' => -1,
    ),
  );
  
  foreach (array_keys(xmedia_display_get_bundles()) as $bundle) {
    $extra['xmedia_display'][$bundle] = array(
      'form' => $fields,
      'display' => $fields,
    );
    xmedia_ui_add_default_fields($bundle);
  }

  return $extra;
}

/**
 * Implements HOOK_entity_insert()
 *
 * @param $entity_type
 * @param $entity
 */
function xmedia_ui_entity_insert($entity, $entity_type) {
  // do something on entity insert
}

/**
 * Implements HOOK_entity_update()
 * @param $entity
 * @param $entity_type
 */
function xmedia_ui_entity_update($entity, $entity_type) {
  // do something on entity update
}

/**
 * Implements hook_views_api().
 */
function xmedia_ui_views_api() {
  return array(
    'api' => 3.0,
    'path' => drupal_get_path('module', 'xmedia_ui'),
  );
}

/**
 * Implements hook_field_create_instance().
 */
function xmedia_ui_field_create_instance($instance) {
  if ($instance['entity_type'] == 'xmedia') {

    $fields = variable_get('fields_in_xmedia_displays', array());
    if (!isset($fields[$instance['field_name']])) {
      return;
    }

    foreach (array_keys(xmedia_display_get_bundles()) as $bundle) {
      $instance['bundle'] = $bundle;
      $instance['entity_type'] = 'xmedia_display';
      field_create_instance($instance);
    }
  }
}

/**
 * Implements hook_field_delete_instance().
 */
function xmedia_ui_field_delete_instance($instance) {
  if ($instance['entity_type'] == 'xmedia') {

    $fields = variable_get('fields_in_xmedia_displays', array());
    if (!isset($fields[$instance['field_name']])) {
      return;
    }

    foreach (array_keys(xmedia_display_get_bundles()) as $bundle) {
      $instance['bundle'] = $bundle;
      $instance['entity_type'] = 'xmedia_display';
      field_delete_instance($instance);
    }

    unset($fields[$form['#field']['field_name']]);
    variable_set('fields_in_xmedia_displays', $fields);
  }
}

/**
 * Implements hook_field_update_instance().
 */
function xmedia_ui_field_update_instance($instance, $prior_instance) {
  if ($instance['entity_type'] == 'xmedia') {

    $fields = variable_get('fields_in_xmedia_displays', array());
    if (!isset($fields[$instance['field_name']])) {
      return;
    }

    foreach (array_keys(xmedia_display_get_bundles()) as $bundle) {

      $query = db_select('field_config_instance', 'fci');
      $query->condition('field_name', $instance['field_name']);
      $query->condition('entity_type', 'xmedia_display');
      $query->condition('bundle', $bundle);
      $query->fields('fci', array('id'));
      $id = $query->execute()->fetchColumn();

      $instance['bundle'] = $bundle;
      $instance['entity_type'] = 'xmedia_display';

      if (empty($id)) {
        field_create_instance($instance);
      }
      else {
        field_update_instance($instance);
      }
    }
  }
}

function xmedia_ui_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'field_ui_field_edit_form' && $form['#instance']['entity_type'] == 'xmedia') {

    $fields = variable_get('fields_in_xmedia_displays', array());

    $form['xmedia_settings'] = array(
      '#type' => 'fieldset',
      '#title' => 'Xmedia settings',
      '#weight' => 0,
    );
    $form['xmedia_settings']['use_in_displays'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use this field in xmedia display type.'),
      '#element_validate' => array('_xmedia_ui_use_in_displays_validate'),
      '#return_value' => 1,
      '#default_value' => (empty($fields[$form['#field']['field_name']]) ? 0 : 1),
    );
  }
}

function _xmedia_ui_use_in_displays_validate($element, &$form_state, $form) {

  $fields = variable_get('fields_in_xmedia_displays', array());
  if ($element['#value']) {
    $fields[$form['#field']['field_name']] = 1;
  }
  else {

    if (isset($fields[$form['#field']['field_name']])) {
      unset($fields[$form['#field']['field_name']]);
    }
  }
  variable_set('fields_in_xmedia_displays', $fields);
}

function xmedia_ui_add_fields_from_xmedia() {
  
  $fields = field_info_instances('xmedia');
  $fields_in_xmedia_displays = variable_get('fields_in_xmedia_displays', array());
  
  foreach($fields as $instances) {
    foreach($instances as $instance) {
      if (isset($fields_in_xmedia_displays[$instance['field_name']])) {
        $instance['bundle'] = 'default';
        $instance['entity_type'] = 'xmedia_display';
        field_create_instance($instance);
      }
    }
  }
}
