<?php

/**
 * @param $form
 * @param $form_state
 * @return array
 */
function xmedia_ui_form_library_upload($form, &$form_statem, $field_name = null) {
  $form = xmedia_library_form_upload($form, $form_state);
  $form['submit']['#submit'][] = 'xmedia_ui_form_library_upload_submit';
  
  if(!empty($field_name)) {
    $form['field_name'] = array(
      '#type' => 'value',
      '#value' => $field_name,
    );
  }

  unset($form['bulk_update']);
  return $form;
}

function xmedia_ui_form_library_upload_submit($from, &$form_state) {
  $redirect_path = 'xmedia-browser';
  
  /* Add field name as second argument to the redirected path */
  if(isset($form_state['values']['field_name'])) {
    $redirect_path .= '/' . $form_state['values']['field_name'];
  }
  
  $form_state['redirect'] = $redirect_path;
}

/**
 * @return array|mixed
 */
function xmedia_ui_library_page($field_name = null) {
  drupal_add_css(drupal_get_path('module', 'xmedia_ui') . '/css/xmedia_ui.css');

  return drupal_get_form('xmedia_ui_form_library_upload', $field_name);
}

/**
 * Page callback
 * uri: admin/xmedia/library
 *
 * @return void
 */
function xmedia_ui_browser_page($field) {
  $is_wizard = FALSE;
    
  drupal_add_css(drupal_get_path('module', 'xmedia_ui') . '/css/xmedia_ui.css');
  drupal_add_js(drupal_get_path('module', 'xmedia_ui') . '/js/xmedia_ui_behavior.js');
  
  if(!empty($field)) {
    if($field == XMEDIA_UI_BROWSER_WIZARD) {
      $is_wizard = TRUE;
      drupal_add_js(array(
        'xmediaBrowser' => array(
          'mode' => 'wizard',
          'selectedImage' => null,
        )
      ), 'setting');
    } 
    else {
      $field_info = field_info_field($field);
      drupal_add_js(array(
        'xmediaBrowser' => array(
          'field' => $field,
          'limit' => $field_info['cardinality'] * 1,
          'mode' => 'widget',
        )
      ), 'setting');
    }
  }

  $embed_view = views_embed_view(variable_get('xmedia_ui_browser_view', 'xmedia_browser'), 'default', arg(1));
  return theme('xmedia_ui_browser', array('content' => $embed_view, 'wizard' => $is_wizard));
}

/**
 * Page callback
 * uri: xmedia-browser/wizard/insert/%
 * 
 * @param type $xmedia_id
 *   ID of xmedia entity with image to insert
 */
function xmedia_ui_insert_page($xmedia_id) {
  drupal_add_css(drupal_get_path('module', 'xmedia_ui') . '/css/xmedia_ui.css');  
  drupal_add_js(array(
    'xmediaBrowser' => array(
      'mode' => 'insert',
    )
  ), 'setting');
  
  $xmedia = xmedia_load($xmedia_id);
    
  return drupal_get_form('xmedia_ui_insert_form', $xmedia);
}

/**
 * Get table of xmedia_display bundles
 * Implementation of page callback admin/structure/xmedia-ui
 * @return string
 */
function xmedia_ui_types() {
  $bundles = xmedia_ui_db_get_bundles();

  if (is_array($bundles)) {
    $header = array(
      array('data' => t('Name')),
      array('data' => t('Operations'), 'colspan' => 4),
    );

    foreach ($bundles as $key => $bundle) {
      $detail = theme('xmedia_type_detail', array(
                                                 'title' => $bundle->title,
                                                 'type' => $bundle->type,
                                                 'description' => isset($bundle->description) ? $bundle->description : ''
                                            ));

      $items[] = array(
        'data' => array(
          $detail,
          l(t('edit'), 'admin/structure/xmedia-ui/manage/' . $bundle->type),
          l(t('manage fields'), 'admin/structure/xmedia-ui/manage/' . $bundle->type . '/fields'),
          l(t('manage display'), 'admin/structure/xmedia-ui/manage/' . $bundle->type . '/display'),
          l(t('delete'), 'admin/structure/xmedia-ui/' . $bundle->type . '/delete'),
        )
      );
    }

    $variables = array(
      'header' => $header,
      'rows' => isset($items) ? $items : array(),
      'empty' => '',
    );

    return theme('table', $variables);
  }

  drupal_set_message(t('There are no media display types yet added'), 'warning');
  return '';
}

/**
 * Creates new media type
 * Implementation of page callback admin/structure/xmedia-ui/add
 * @return string
 */
function xmedia_ui_types_add() {
  return drupal_get_form('xmedia_ui_form_type');
}
