<?php

/**
 * Implements HOOK_theme_registry_alter()
 * @param $theme_registry
 */
function xmedia_ui_theme_registry_alter(&$theme_registry) {

  $theme_registry['html__xmedia_browser'] = Array();
  $theme_registry['html__xmedia_browser']['template'] = 'html--xmedia-browser';
  $theme_registry['html__xmedia_browser']['path'] = drupal_get_path('module', 'xmedia_ui') . "/theme";
  $theme_registry['html__xmedia_browser']['render element'] = 'page';
  $theme_registry['html__xmedia_browser']['base hook'] = 'html';
  $theme_registry['html__xmedia_browser']['type'] = 'theme_engine';
  $theme_registry['html__xmedia_browser']['theme path'] = path_to_theme();
  $theme_registry['html__xmedia_browser']['preprocess functions'] = array();
  $theme_registry['html__xmedia_browser']['process functions'] = array();

  $theme_registry['page__xmedia_browser'] = Array();
  $theme_registry['page__xmedia_browser']['template'] = 'page--xmedia-browser';
  $theme_registry['page__xmedia_browser']['path'] = drupal_get_path('module', 'xmedia_ui') . "/theme";
  $theme_registry['page__xmedia_browser']['render element'] = 'page';
  $theme_registry['page__xmedia_browser']['base hook'] = 'page';
  $theme_registry['page__xmedia_browser']['type'] = 'theme_engine';
  $theme_registry['page__xmedia_browser']['theme path'] = path_to_theme();
  $theme_registry['page__xmedia_browser']['preprocess functions'] = array();
  $theme_registry['page__xmedia_browser']['process functions'] = array();

  $theme_registry['html__xmedia_browser_upload'] = Array();
  $theme_registry['html__xmedia_browser_upload']['template'] = 'html--xmedia-browser-upload';
  $theme_registry['html__xmedia_browser_upload']['path'] = drupal_get_path('module', 'xmedia_ui') . "/theme";
  $theme_registry['html__xmedia_browser_upload']['render element'] = 'page';
  $theme_registry['html__xmedia_browser_upload']['base hook'] = 'html';
  $theme_registry['html__xmedia_browser_upload']['type'] = 'theme_engine';
  $theme_registry['html__xmedia_browser_upload']['theme path'] = path_to_theme();
  $theme_registry['html__xmedia_browser_upload']['preprocess functions'] = array();
  $theme_registry['html__xmedia_browser_upload']['process functions'] = array();

  $theme_registry['page__xmedia_browser_upload'] = Array();
  $theme_registry['page__xmedia_browser_upload']['template'] = 'page--xmedia-browser-upload';
  $theme_registry['page__xmedia_browser_upload']['path'] = drupal_get_path('module', 'xmedia_ui') . "/theme";
  $theme_registry['page__xmedia_browser_upload']['render element'] = 'page';
  $theme_registry['page__xmedia_browser_upload']['base hook'] = 'page';
  $theme_registry['page__xmedia_browser_upload']['type'] = 'theme_engine';
  $theme_registry['page__xmedia_browser_upload']['theme path'] = path_to_theme();
  $theme_registry['page__xmedia_browser_upload']['preprocess functions'] = array();
  $theme_registry['page__xmedia_browser_upload']['process functions'] = array();
}

/**
 * Implementation of HOOK_theme
 * @return array
 */
function xmedia_ui_theme() {
  return array(
    'xmedia_ui_library_wrapper' => array(
      'variables' => array('title' => NULL),
      'template' => 'theme/xmedia_ui_library_wrapper',
    ),
    'xmedia_ui_browser' => array(
      'variables' => array('content' => NULL, 'wizard' => NULL),
      'template' => 'theme/xmedia_ui_browser'
    ),
    'xmedia_ui_library' => array(
      'variables' => array('xmedias'),
      'template' => 'theme/xmedia_ui_library',
    ),
    'xmedia_multiple_value_form' => array(
      'render element' => 'element',
    ),
    'xmedia_ui_element' => array(
      'render element' => 'element',
    ),
    'xmedia_display' => array(
      'render element' => 'elements',
      'template' => 'theme/xmedia_display',
    ),
  );
}

/**
 * Preprocess theme for library wrapper
 * @param $variables
 */
function template_preprocess_xmedia_ui_library_wrapper(&$variables) {
  $variables['list'] = theme('xmedia_ui_library', array('xmedias' => xmedia_db_get_all_media()));
}

/**
 * Preprocess theme for upload browser
 * @param $variables
 */
function template_preprocess_xmedia_ui_browser(&$variables) {
  $actions = array();
  
  if($variables['wizard'] == FALSE) {
    $actions[] = l(t('Add images'), '', array(
      'attributes' => array(
        'id' => 'xmedia-add-images',
        'class' => array(
          'button', 'form-submit'
        ),
      ),
      'fragment' => 'refresh'
    ));

    $actions[] = l(t('Close browser'), '', array(
      'attributes' => array(
        'id' => 'xmedia-cancel-images',
        'class' => array(
          'button', 'form-submit'
        ),
      ),
      'fragment' => 'refresh'
    ));
  }

  $variables['actions'] = $actions;
}

/**
 * Preprocess theme for library list
 * @param $variables
 */
function template_preprocess_xmedia_ui_library(&$variables) {
  $xmedias = $variables['xmedias'];

  $display = array();

  foreach ($xmedias as $delta => $xmedia) {
    $display[$delta]['content'] = drupal_render(xmedia_view(array($xmedia), 'library'));
    $display[$delta]['entity'] = $xmedia;
  }

  $rows = array_chunk($display, 3);
  $variables['rows'] = $rows;
}


/**
 * Custom implementation for field widgets with unlimited cardinality
 *
 * @param $variables
 * @return string
 */
function theme_xmedia_multiple_value_form($variables) {

  $element = $variables['element'];
  $output = '';

  $delta = ($element['#max_delta']);
  
  $table_id = drupal_html_id($element['#field_name'] . '_values');
  $order_class = $element['#field_name'] . '-delta-order';
  $required = !empty($element['#required']) ? theme('form_required_marker', $variables) : '';
  $draggable = $element['#cardinality'] == 1 ? false : true;

  $header = array(
    array(
      'data' => '<label>' . t('!title: !required', array(
        '!title' => $element['#title'],
        '!required' => $required
      )) . "</label>",
      'colspan' => 2,
      'class' => array('field-label'),
    ),
  );
  if($draggable) {
    $header[] = t('Order');
  }
  
  $rows = array();

  // Sort items according to '_weight' (needed when the form comes back after
  // preview or failed validation)
  $items = array();
  $static_elements = array();
  foreach (element_children($element) as $key) {
    // in php 0 == "string" as true ? we are using === instead of ==
    // switch not possible
    if ($key === 'add_more' || $key === 'mid_elements' || $key === 'browse') {
      $static_elements[$key] = drupal_render($element[$key]);
    }
    else {
      $items[$key] = $element[$key];
    }
  }

  usort($items, '_field_sort_items_value_helper');

  // Add the items as table rows.
  foreach ($items as $key => $item) {
    $item['_weight']['#attributes']['class'] = array($order_class);
    $delta_element = drupal_render($item['_weight']);
    $classes = array('field-multiple-drag', 'xmedia-remove');

    if ($key == $delta) {
      $classes[] = 'xmedia-remove-hidden';
    }

    $cells = array(
      array('data' => drupal_render($item['_remove']), 'class' => $classes),
      drupal_render($item),
    );
    
    if($draggable) {
      $cells[] = array('data' => $delta_element, 'class' => array('delta-order'));
      $rows[] = array(
        'data' => $cells,
        'class' => array('draggable'),
      );
    } else {
      $rows[] = array(
        'data' => $cells,
      );      
    }
  }

  $output = '<div class="form-item">';
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id, 'class' => array('field-multiple-table'))
  ));
  $output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
  $output .= '<div class="clearfix">' . implode(' ', $static_elements) . '</div>';
  $output .= '</div>';
  
  if($draggable) {
    drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
  }

  return $output;
}

