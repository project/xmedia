<?php

/**
 * @file
 * Views integration for jCarousel module.
 */

require_once 'xmedia_ui_style_plugin.inc';


/**
 * Implements hook_views_plugins().
 */
function xmedia_ui_views_plugins() {

  $plugins['style']['xmedia'] = array(
    'theme' => 'xmediaui_view',
    'theme path' => drupal_get_path('module', 'xmedia_ui'),
    'path' => drupal_get_path('module', 'xmedia_ui'),
    'file' => 'xmedia_ui_style_plugin.inc',
    'title' => t('XMedia selector'),
    'handler' => 'xmedia_ui_style_plugin',
    'uses options' => FALSE,
    'uses grouping' => FALSE,
    'uses row plugin' => TRUE,
    'uses fields' => TRUE,
    'type' => 'normal',
  );

  return $plugins;
}


/**
 * @param $variables
 */
function template_preprocess_xmediaui_view(&$variables) {
  $view = $variables['view'];
  $ids = array();

  // Identifier of xmedia is required -> sending it to template
  foreach($view->result as $item) {
    $ids[] = $item->id;
  }

  $variables['ids'] = $ids;
}
