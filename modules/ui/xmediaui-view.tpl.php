<?php
/**
 * Browse view
 */
?>

<div id="xmedia-ui-library">
  <div id="xmedia-ui-library-wrapper">
    <div id="xmedia-ui-library-content">
      <div id="xmedia-ui-library-content-wrapper">
        <div id="xmedia-ui-library-list-wrapper" class="clearfix">
          <?php foreach ($rows as $delta => $row): ?>
            <div id="<?php print $ids[$delta]; ?>" class="xmedia-selectable xmedia-not-selected">
              <?php print $row; ?>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</div>
