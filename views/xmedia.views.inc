<?php

/**
 * Implements HOOK_views_default_views().
 */
function xmedia_views_default_views() {
  $views = array();

  $view_library = new view;
  $view_library->name = 'xmedia_library';
  $view_library->description = '';
  $view_library->tag = 'default';
  $view_library->base_table = 'xmedias';
  $view_library->human_name = 'Xmedia library';
  $view_library->core = 7;
  $view_library->api_version = '3.0';
  $view_library->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view_library->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'XMedia library';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access xmedia library';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'id'              => 'id',
    'alternate_text'  => 'alternate_text',
    'created'         => 'created',
    'modified'        => 'modified',
    'nothing'         => 'edit_link',
    'edit_link'       => 'edit_link',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'alternate_text' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'modified' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_link' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => ' ',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Relationship: XMedia: Author */
  $handler->display->display_options['relationships']['user_id']['id'] = 'user_id';
  $handler->display->display_options['relationships']['user_id']['table'] = 'xmedias';
  $handler->display->display_options['relationships']['user_id']['field'] = 'user_id';
  $handler->display->display_options['relationships']['user_id']['required'] = 0;
  /* Field: XMedia: Xmedia ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'xmedias';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['id']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['id']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['id']['format_plural'] = 0;
  $handler->display->display_options['fields']['id']['separator'] = '';
  /* Field: Bulk operations: XMedia */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'xmedias';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['empty_zero'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['operations'] = array(
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_message_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::system_goto_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['display_result'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['merge_single_action'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['force_single'] = 0;
  /* Field: Field: Xmedia image */
  $handler->display->display_options['fields']['xmedia_image']['id'] = 'xmedia_image';
  $handler->display->display_options['fields']['xmedia_image']['table'] = 'field_data_xmedia_image';
  $handler->display->display_options['fields']['xmedia_image']['field'] = 'xmedia_image';
  $handler->display->display_options['fields']['xmedia_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['xmedia_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['xmedia_image']['field_api_classes'] = 0;
  /* Field: XMedia: Label */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'xmedias';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'xmedia/[id]/edit';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['alt'] = '[title]';
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'user_id';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['name']['link_to_user'] = 1;
  $handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
  $handler->display->display_options['fields']['name']['format_username'] = 1;
  /* Field: XMedia: Created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'xmedias';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['created']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Field: XMedia: Modified */
  $handler->display->display_options['fields']['modified']['id'] = 'modified';
  $handler->display->display_options['fields']['modified']['table'] = 'xmedias';
  $handler->display->display_options['fields']['modified']['field'] = 'modified';
  $handler->display->display_options['fields']['modified']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['external'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['modified']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['modified']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['modified']['alter']['html'] = 0;
  $handler->display->display_options['fields']['modified']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['modified']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['modified']['hide_empty'] = 0;
  $handler->display->display_options['fields']['modified']['empty_zero'] = 0;
  $handler->display->display_options['fields']['modified']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['modified']['date_format'] = 'short';
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'user_id';
  $handler->display->display_options['filters']['uid']['value'] = '';

  /* Field: Remove */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Remove';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'xmedia/[id]/delete';

  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'views';
  $handler->display->display_options['fields']['edit_link']['field'] = 'nothing';
  $handler->display->display_options['fields']['edit_link']['label'] = 'Operations';
  $handler->display->display_options['fields']['edit_link']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['edit_link']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['edit_link']['alter']['path'] = 'xmedia/[id]/edit';

  /* Display: Page */
  $handler = $view_library->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/structure/xmedia-library';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Xmedia library';
  $handler->display->display_options['menu']['description'] = 'List of uploaded xmedia files';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;

  $views['xmedia_library'] = $view_library;

  $xmedia_browser = new view;
  $xmedia_browser->name = 'xmedia_browser';
  $xmedia_browser->description = '';
  $xmedia_browser->tag = 'default';
  $xmedia_browser->base_table = 'xmedias';
  $xmedia_browser->human_name = 'Xmedia browser';
  $xmedia_browser->core = 7;
  $xmedia_browser->api_version = '3.0';
  $xmedia_browser->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $xmedia_browser->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Xmedia browser';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '9';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'xmedia';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: XMedia: Author */
  $handler->display->display_options['relationships']['user_id']['id'] = 'user_id';
  $handler->display->display_options['relationships']['user_id']['table'] = 'xmedias';
  $handler->display->display_options['relationships']['user_id']['field'] = 'user_id';
  $handler->display->display_options['relationships']['user_id']['required'] = 0;
  /* Field: XMedia: Xmedia ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'xmedias';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['label'] = '';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['id']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['id']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['id']['format_plural'] = 0;
  /* Field: Field: Xmedia image */
  $handler->display->display_options['fields']['xmedia_image']['id'] = 'xmedia_image';
  $handler->display->display_options['fields']['xmedia_image']['table'] = 'field_data_xmedia_image';
  $handler->display->display_options['fields']['xmedia_image']['field'] = 'xmedia_image';
  $handler->display->display_options['fields']['xmedia_image']['label'] = '';
  $handler->display->display_options['fields']['xmedia_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['xmedia_image']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['xmedia_image']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['xmedia_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['xmedia_image']['settings'] = array(
    'image_style' => 'medium',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['xmedia_image']['field_api_classes'] = 0;
  /* Field: XMedia: Label */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'xmedias';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Title';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'user_id';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['name']['link_to_user'] = 0;
  $handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
  $handler->display->display_options['fields']['name']['format_username'] = 1;
  /* Field: XMedia: Caption */
  $handler->display->display_options['fields']['caption']['id'] = 'caption';
  $handler->display->display_options['fields']['caption']['table'] = 'xmedias';
  $handler->display->display_options['fields']['caption']['field'] = 'caption';
  $handler->display->display_options['fields']['caption']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['external'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['caption']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['caption']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['caption']['alter']['html'] = 0;
  $handler->display->display_options['fields']['caption']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['caption']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['caption']['empty'] = 'No caption provided';
  $handler->display->display_options['fields']['caption']['hide_empty'] = 0;
  $handler->display->display_options['fields']['caption']['empty_zero'] = 0;
  $handler->display->display_options['fields']['caption']['hide_alter_empty'] = 1;
  /* Field: XMedia: Alternate_text */
  $handler->display->display_options['fields']['alternate_text']['id'] = 'alternate_text';
  $handler->display->display_options['fields']['alternate_text']['table'] = 'xmedias';
  $handler->display->display_options['fields']['alternate_text']['field'] = 'alternate_text';
  $handler->display->display_options['fields']['alternate_text']['label'] = 'Alternate text';
  $handler->display->display_options['fields']['alternate_text']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['external'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['alternate_text']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['alternate_text']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['alternate_text']['alter']['html'] = 0;
  $handler->display->display_options['fields']['alternate_text']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['alternate_text']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['alternate_text']['empty'] = 'No alternate text provided';
  $handler->display->display_options['fields']['alternate_text']['hide_empty'] = 0;
  $handler->display->display_options['fields']['alternate_text']['empty_zero'] = 0;
  $handler->display->display_options['fields']['alternate_text']['hide_alter_empty'] = 1;
  /* Field: XMedia: Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'xmedias';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['description']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['description']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['description']['alter']['external'] = 0;
  $handler->display->display_options['fields']['description']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['description']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['description']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['description']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['description']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['description']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['description']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['description']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['description']['alter']['html'] = 0;
  $handler->display->display_options['fields']['description']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['description']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['description']['empty'] = 'No description provided';
  $handler->display->display_options['fields']['description']['hide_empty'] = 0;
  $handler->display->display_options['fields']['description']['empty_zero'] = 0;
  $handler->display->display_options['fields']['description']['hide_alter_empty'] = 1;
  /* Filter criterion: User: Name (raw) */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'users';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'user_id';
  $handler->display->display_options['filters']['name']['group'] = 1;
  $handler->display->display_options['filters']['name']['exposed'] = TRUE;
  $handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['label'] = 'Name (raw)';
  $handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
  $handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
  $handler->display->display_options['filters']['name']['expose']['required'] = 0;
  $handler->display->display_options['filters']['name']['expose']['multiple'] = FALSE;

  /* Display: Page */
  $handler = $xmedia_browser->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/xmedia/browser';

  $views['xmedia_browser'] = $xmedia_browser;

  return $views;
}
