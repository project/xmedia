<?php

function xmedia_db_get_media_list($search = NULL) {
  // Header array for table. It is function cause I need it on multiple places.
  $header = xmedia_library_list_header();

  if (!empty($search)) {
    $or = db_or()->condition('x.title', '%' . $search . '%', 'like')->condition('u.name', '%' . $search . '%', 'like');

    // Get paged list of all media files
    $medias = db_select('xmedias', 'x');
    $medias->fields('x');
    $medias->leftJoin('users', 'u', 'u.uid = x.user_id');
    $medias->condition($or);
    $medias = $medias->extend('PagerDefault')->limit(XMEDIA_PAGER_SIZE)
      ->extend('TableSort')->orderByHeader($header);
    $medias->orderBy('x.created', 'DESC');
    $medias = $medias->execute()->fetchAll();

  }
  else {
    // Get paged list of all media files
    $medias = db_select('xmedias', 'x')
      ->fields('x')
      ->extend('PagerDefault')->limit(XMEDIA_PAGER_SIZE)
      ->extend('TableSort')->orderByHeader($header)
      ->orderBy('created', 'DESC')
      ->execute()
      ->fetchAll();
  }

  return $medias;
}

/**
 * Load media by fid
 *
 * @param $fid
 * @return XMedia
 */
function xmedia_db_get_media_by_fid($fid) {
  return entity_get_controller('xmedia')->loadByFid($fid);
}

/**
 * @param $id
 * @return XMedia
 */
function xmedia_load($id) {

  $id = str_replace('%2B', ' ', $id);
  $id = str_replace('+', ' ', $id);
  $parts = explode(' ', $id);

  if (is_array($parts) && sizeof($parts) > 1) {
    $ids = $parts;
    return xmedia_load_multiple($ids);
  }
  
  $xmedias = xmedia_load_multiple(array($id));

  return array_shift($xmedias);
}

/**
 * @param $type
 * @return array|mixed
 */
function xmedia_type_load($type) {
  return xmedia_get_bundles($type);
}

/**
 * @param $ids
 * @return mixed
 */
function xmedia_load_multiple($ids) {
  return entity_load('xmedia', $ids);
}

/**
 * @param $media
 * @return bool
 */
function xmedia_delete($media) {
  return entity_delete('xmedia', entity_id('xmedia', $media));
}

/**
 * @param $media
 * @return bool
 */
function xmedia_type_delete($media) {
  return entity_delete('xmedia_type', entity_id('xmedia_type', $media));
}

/**
 * @param array $ids
 * @return bool
 */
function xmedia_delete_multiple(array $ids) {
  return entity_delete_multiple('xmedia', $ids);
}

/**
 * @param array $ids
 * @return bool
 */
function xmedia_type_delete_multiple(array $ids) {
  return entity_delete_multiple('xmedia_type', $ids);
}


/**
 * Save XMedia entity
 *
 * @param XMedia $media
 * @return mixed
 */
function xmedia_save($media) {
  return entity_save('xmedia', $media);
}

/**
 * Save XMediaType entity
 *
 * @param $media_type
 * @return mixed
 */
function xmedia_type_save($media_type) {
  return entity_save('xmedia_type', $media_type);
}

/**
 * Get all XMedia entities
 *
 * @return array
 */
function xmedia_db_get_all_media() {
  $query = db_select('xmedias', 'x')
    ->fields('x', array('id'));

  return entity_get_controller('xmedia')->load($query->execute()->fetchCol());
}

/**
 * Save file object(plupload_file_uri_to_object) as xmedia entity
 *
 * @param stdClass $file
 * @param string $type bundle type
 * @return FALSE|XMedia
 */
function xmedia_db_save_media_from_file(stdClass $file, $type = NULL) {
  $file = file_save($file);

  $media = entity_create('xmedia',
    array(
      'fid' => $file->fid,
      'title' => $file->filename,
    )
  );

  if ($type != NULL) {
    $media->type = $type;
  }

  $info = array();
  $data = getimagesize(drupal_realpath($file->uri), $info);
  if (isset($info['APP13'])) {
    $iptc = iptcparse($info['APP13']);
  }


  $media->xmedia_image[LANGUAGE_NONE][] = array(
    'fid' => $file->fid,
  );

  if(xmedia_save($media)) {
    return $media;
  }

  return FALSE;
}

/**
 * @param null $type_name
 * @return array|mixed
 */
function xmedia_get_bundles($type_name = NULL) {
  $types = entity_load_multiple_by_name('xmedia_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}


/**
 * Get bundle options array of select or
 * only title of bundle if argument $index present
 * @param null $index
 * @return array|string
 */
function xmedia_db_get_bundle_options($index = NULL) {
  $options = db_select('xmedia_type', 't')
    ->fields('t', array('type', 'title'))
    ->execute()->fetchAllKeyed();

  if (!empty($index) && isset($options[$index])) {
    return $options[$index];
  }

  return $options;
}

/**
 * Get keyed version of all bundles
 *
 * @return array
 */
function xmedia_db_get_bundles_keyed() {
  $bundles = xmedia_get_bundles();
  $keyed = array();

  if (is_array($bundles)) {
    foreach ($bundles as $bundle) {
      $keyed[$bundle->type] = $bundle->title;
    }
  }

  return $keyed;
}


/**
 * @return mixed
 */
function xmedia_db_get_all_types() {
  $query = db_select('xmedia_type', 'xt')
    ->fields('xt', array());

  return $query->execute()->fetchAll();
}