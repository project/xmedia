<?php

/**
 * Xmedia core settings form
 * @param $form
 * @param $form_state
 * @return array
 */
function xmedia_form_settings($form , &$form_state) {
  $form['xmedia_library_path'] = array(
    '#type' => 'textfield',
    '#description' => t('Path to xmedia library view'),
    '#default_value' => variable_get('xmedia_library_path', 'admin/structure/xmedia-library'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * XMedia form - create/edit
 * @param $form
 * @param $form_state
 * @param $xmedia
 * @return array
 */
function xmedia_form_media($form, &$form_state, $xmedia) {
  $form = array();

  $form['xmedia'] = array(
    '#type' => 'value',
    '#value' => $xmedia,
  );

//  $form['image_info']['image_url'] = array(
//    '#title' => t('Url of file'),
//    '#type' => 'textfield',
//    '#value' => file_create_url($file->uri),
//    '#disabled' => TRUE,
//  );

  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => isset($xmedia->title) ? $xmedia->title : '',
  );

  $form['alternate_text'] = array(
    '#title' => t('Alternate text'),
    '#type' => 'textfield',
    '#default_value' => isset($xmedia->alternate_text) ? $xmedia->alternate_text : '',
  );

  $form['caption'] = array(
    '#title' => t('Caption'),
    '#type' => 'textfield',
    '#default_value' => isset($xmedia->caption) ? $xmedia->caption : '',
  );

  $form['actions'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('xmedia_form_media_submit'),
    '#weight' => 0,
  );

  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#attributes' => array(
      'class' => array('button'),
    ),
    '#markup' => l(t('Cancel'), variable_get('xmedia_library_path', 'admin/structure/xmedia-library'),
      array(
        'attributes' => array('class' => array('button')),
    )),
    '#weight' => 2,
  );

  if(!empty($xmedia->id)) {
    $form['actions']['remove'] = array(
      '#type' => 'markup',
      '#markup' => l(t('Remove'), 'xmedia/' . $xmedia->id. '/delete',
        array(
          'destination' => variable_get('xmedia_library_path', 'admin/structure/xmedia-library'),
          'attributes' => array('class' => array('button')),
      )),
      '#weight' => 1,
    );
  }

  field_attach_form('xmedia', $xmedia, $form, $form_state);
  return $form;
}

function xmedia_form_media_validate($form, &$form_state) {

  $xmedia = $form_state['values']['xmedia'];
  field_attach_form_validate('xmedia', $xmedia, $form, $form_state);
}

/**
 * Confirm delete - form callback
 *
 * @param $form
 * @param $form_state
 * @param $xmedia
 * @return array
 */
function xmedia_form_delete_confirm($form, &$form_state, $xmedia) {

  $form['xmedia'] = array(
    '#type' => 'value',
    '#value' => $xmedia,
  );
  
  $list_of_nodes = array(
      'header' => array (
        '#markup' => '<h3>' . t('Xmedia %title is attached to this nodes:', array('%title' => $xmedia->title)) . '</h3>',
      ),'items' => array (
      '#theme' => 'item_list',
      '#items' => xmedia_list_nodes_by_xmedias(array($xmedia->id))));


  return confirm_form(
    $form,
    t('Are you sure you want to delete xmedia %title?', array('%title' => $xmedia->title)),
    '',
    t('This action cannot be undone.') . drupal_render($list_of_nodes),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Confirm delete - submit callback
 *
 * @param $form
 * @param $form_state
 */
function xmedia_form_delete_confirm_submit($form, &$form_state) {
  $xmedia = $form_state['values']['xmedia'];

  if(xmedia_delete($xmedia)) {
    drupal_set_message('Xmedia file %title was deleted', array('%title' => $xmedia->title));
  }

  $form_state['redirect'] = variable_get('xmedia_library_path', 'admin/structure/xmedia-library');
}

/**
 * @param $form
 * @param $form_state
 */
function xmedia_form_media_submit($form, &$form_state) {
  $data = $form_state['values'];
  $xmedia = $data['xmedia'];

  // attach field submit
  field_attach_submit('xmedia', $xmedia, $form, $form_state);

  // assign values to entity properties
  $xmedia->title = $data['title'];
  $items = field_get_items('xmedia', $xmedia, 'xmedia_image');
  $xmedia->fid = $items[0]['fid'];
  $xmedia->alternate_text = $data['alternate_text'];
  $xmedia->caption = $data['caption'];
  $xmedia->description = $data['description'];

  if (xmedia_save($xmedia)) {
    drupal_set_message(t('Media file successfully saved.'), 'status');
    $form_state['redirect'] = variable_get('xmedia_library_path', 'admin/structure/xmedia-library');
  }
  else {
    drupal_set_message(t('Can not save media file!'), 'error');
  }
}

/**
 *
 * @param $form
 * @param $form_state
 * @param XMediaType $bundle
 * @return array
 */
function xmedia_type_form($form, &$form_state, XMediaType $bundle = NULL) {
  $form = array();

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => isset($bundle->id) ? $bundle->id : '',
  );

  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('The human-readable name of this content type. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#default_value' => isset($bundle->title) ? $bundle->title : '',
  );

  $form['type'] = array(
    '#title' => t('Type'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('The machine-readable name of this content type. It is recommended that this code contain only letters, numbers, and underscores. This name must be unique.'),
    '#default_value' => isset($bundle->type) ? $bundle->type : '',
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#description' => t('Describe this media type. The text will be displayed on the Add new media.'),
    '#default_value' => isset($bundle->description) ? $bundle->description : '',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save media type'),
    '#submit' => array('xmedia_type_form_submit'),
  );

  return $form;
}

function xmedia_type_form_submit($form, &$form_state) {
  $model_type = entity_ui_form_submit_build_entity($form, $form_state);
  $model_type->save();

  $form_state['redirect'] = 'admin/structure/xmedia';
}


/**
 * Implements hook_views_bulk_operations_form_alter()
 */
function xmedia_views_bulk_operations_form_alter(&$form, &$form_state, $vbo) {
  if($vbo->view->name == 'xmedia_library' && $form_state['step'] == 'views_bulk_operations_confirm_form') {
    $form['message'] = array(
      'header' => array (
        '#markup' => '<h3>' . t('Which are attached to this nodes:') . '</h3>',
      ),
       'items' => array (
      '#theme' => 'item_list',
      '#items' => xmedia_list_nodes_by_xmedias($form_state['values']['views_bulk_operations']),)
    );
  }
}

/**
 * List nodes with attached xmedias
 * @param array $xmedias
 * return array
 * 
 */
function xmedia_list_nodes_by_xmedias($xmedias) {
    
  $select = db_select('field_config', 'fc');
  $select->fields('fc', array('field_name'));
  $select->condition('type', 'xmedia','=');
  $result = $select->execute();
  $nodes = array();
  
  foreach($result as $field) {
    
    $select = db_select('field_data_' . $field->field_name, 'fd');
    $select->fields('fd', array('entity_type', 'bundle', 'entity_id', $field->field_name . '_mid'));
    $select->condition($field->field_name . '_mid', $xmedias, 'IN');
    $result_entities = $select->execute();
    
    foreach($result_entities as $row) {
      $property = $field->field_name . '_mid';
      $nodes[$row->$property][$row->entity_type][] = $row->entity_id;
    }
  }
  return xmedia_prepare_list_nodes($nodes);
}

/**
 * Crete array with list items for rendering
 * @param array $nodes
 * return array
 * 
 */
function xmedia_prepare_list_nodes($nodes) {
  $list = array();
  foreach($nodes as $xmedia_id => $nodes) {
    $enity = xmedia_load($xmedia_id);
    $list_item = array();
    $children = array();
    foreach($nodes as $entity_type => $node_ids) {
      $ids = array_unique($node_ids);
      foreach($ids as $id) {
        $entity = entity_load($entity_type, array($id));
        $children[]['data'] = $entity[$id]->title;
      }
    }
        
    $list_item['data'] = $enity->title;
    $list_item['children'] = $children;
        $list[] = $list_item;
  }
  return $list;
}