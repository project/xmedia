<?php

/**
 * Title callback
 *
 * @path: xmedia/%xmedia/edit
 * @param XMediaEntity $xmedia
 * @return string
 */
function xmedia_page_edit_title($xmedia) {
  return t('Edit !title', array('!title' => $xmedia->title));
}

/**
 * Title callback
 *
 * @path: xmedia/%xmedia/delete
 * @param XMediaEntity $xmedia
 * @return string
 */
function xmedia_page_delete_title($xmedia) {
  return t('Delete !title', array('!title' => $xmedia->title));
}