<?php
/**
 * Implements hook_theme()
 */
function xmedia_theme() {
  return array(
    'xmedia_type_detail' => array(
      'variables' => array('label' => NULL, 'name' => NULL, 'description' => NULL),
      'template' => 'theme/xmedia-type-detail',
    ),
    'xmedia' => array(
      'render element' => 'elements',
      'template' => 'theme/xmedia',
    ),
    'xmedia_meta' => array(
      'variables' => array('xmedia'),
      'template' => 'theme/xmedia-meta',
    ),
  );
}

/**
 * Preprocess theme for xmedia-type-detail.tpl.php
 */
function template_preprocess_xmedia_type_detail(&$variables) {
  $variables['machine_name'] = t('Machine name: %type', array('%type' => $variables['type']));
}