<?php

function xmedia_convert_file_to_media($file) {
  $xmedia = new XMedia();
  $xmedia->title = $file->filename;
  $xmedia->fid = $file->fid;

  return $xmedia;
}

/**
 * Convert array to object
 *
 * @param array $array
 * @return stdClass
 */
function xmedia_array_to_object(array $array) {
  $object = new stdClass();

  foreach ($array as $key => $value) {
    $object->$key = $value;
  }

  return $object;
}

function xmedia_get_image_styles_options() {
  $styles = array();
  foreach(image_styles() as $style => $settings) {
    $styles[$style] = $settings['name'];
  }
  return $styles;
}